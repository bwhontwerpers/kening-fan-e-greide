<?php /* Nieuws archive */ get_header(); ?>
	
	<?php
	$articles = array(); // Storage container for articles ($articles[yyyy][0] = post)
	$archiveQuery = new WP_Query(array('post_type'=>'post','posts_per_page'=>-1));
	while ($archiveQuery->have_posts()) {
		$archiveQuery->the_post();
		$archiveYear = date('Y', strtotime($post->post_date));
		if (!isset($articles[$archiveYear])) { $articles[$archiveYear] = array(); }
		$articles[$archiveYear][] = $post;
	} wp_reset_query();
	?>
	
	<?php if (is_user_logged_in()) { ?><pre>archive.php</pre><?php } ?>
	
	<div class="row comp_layout comp_news_articles">
		<div class="section">
			<?php foreach ($articles as $year => $posts) { ?>
				<div class="col col_16">
					<h2><?php echo $year; ?></h2>
				</div>
				<div class="col col_16 comp_articles">
				<?php
				foreach ($posts as $post) {
					setup_postdata($post);
					get_template_part("modules/comp","loop-post");
				}
				?>
				</div>
			<?php } wp_reset_postdata(); ?>
		</div>
	</div>		
		
<?php get_footer(); ?>