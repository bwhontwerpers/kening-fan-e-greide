<?php /* Single item displayed in loop: nieuws */ ?>

<?php
/* Get term + icon */
$terms = wp_get_post_terms(get_the_ID(),'subject');
foreach ($terms as $term) {
	$post_icon = get_field('subject-icon','subject_'.$term->term_id);
	if ($post_icon) {
		$post_icon = file_get_contents($post_icon['url']);//bstcm_load_svg(basename($post_icon['url']),false,false);
		$post_icons .= $post_icon;
	}
}
?>

<div class="content__filter__item">
	<a href="<?php the_permalink(); ?>">
		
		<?php if (has_post_thumbnail()) { ?>
		<div class="content__filter__item__thumbnail">
			<?php the_post_thumbnail('nieuws-loop-thumbnail'); ?>
		</div>
		<?php } ?>

		<h2 class="h2--large"><?php the_title(); ?></h2>

		<div class="content__filter__item__metatop">
			<span class="content__filter__item__metatop--category">Nieuws &nbsp;&bull; </span><span class="content__filter__item__metatop--date"><?php echo get_the_date(); ?> &nbsp;&nbsp; </span><div class="content__filter__item__icons"><?php echo $post_icons; ?></div>
		</div>
		<?php the_excerpt(); ?>
		
		<div class="content__filter__item__metabottom">
			<?php
			$locations = "";
			while (has_sub_field('post-locations')) {
				$details = get_sub_field('post-location');
				$locations .= $details['address'];
			}
			if ($locations) { 
				$location = $locations;
			}
			?>
			<?php echo $location; ?>
		</div>
		

	</a>
</div>