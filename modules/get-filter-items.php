<?php
global $customfilter;
$output_string = "";
$output_array = array();

/* Get filter parameters */
//if (isset($_POST['category'])) { $customfilter = $_POST['category']; } else { $customfilter = false; }
//if (isset($_GET['filter'])) { $customfilter = $_GET['filter']; } else { $customfilter = false; }
if (isset($_POST['category'])) { 
	$customfilter = $_POST['category']; 
} elseif (isset($_GET['filter'])) {
	 $customfilter = $_GET['filter'];
} else { 
	$customfilter = "uitgelicht"; 
}

if (!isset($_POST['offset'])) { $customoffset = 0; } else { $customoffset = $_POST['offset']; }
if (isset($_POST['output'])) { $customOutput = $_POST['output']; } else { $customOutput = false; }

if (bwh_get_region_session()) {
	$taxQuery = array(
					'relation'=>'AND',
			        array(
			            'taxonomy' => 'regio',
			            'field' => 'slug',
			            'terms' => array(bwh_get_region_session()),
			            'operator' => 'IN',
			        ),
				    array(
			            'taxonomy' => 'category',
			            'field' => 'slug',
			            'terms' => array($customfilter),
			        )
			    );	    
			    
	/* Run WP query to get items */
	$itemsQuery = new WP_Query(
		array('post_type'=>'post',
			  'tax_query'=>$taxQuery,
			  'posts_per_page'=>14,
			  'post_status' => 'publish',
			  'orderby'=>'date',
			  'order'=>'DESC',
			  'offset'=>$customoffset,
		)
	);

} else {
	$taxQuery = array();
	
	/* Run WP query to get items */
	$itemsQuery = new WP_Query(
		array('post_type'=>'post',
			  'posts_per_page'=>14,
			  'category_name'=>$customfilter,
			  'post_status' => 'publish',			  
			  'orderby'=>'date',
			  'order'=>'DESC',
			  'offset'=>$customoffset,
		)
	);

}

while ($itemsQuery->have_posts()) { 
	$itemsQuery->the_post(); 
	$itemTerms = wp_get_post_terms(get_the_ID(),'category');
	foreach ($itemTerms as $itemTerm) { if ($itemTerm->term_id != 2) { break; } }
	
	if ($customOutput == "json") {
		$output_array[] = load_template_part("modules/loop","post-{$itemTerm->slug}");
	} else {
		$output_string .= "<div class='filter__item__wrapper filter__item__wrapper--".$itemTerm->slug."'>".load_template_part("modules/loop","post-{$itemTerm->slug}")."</div>";
	}
}

if ($customOutput == "json") {
	echo json_encode($output_array);
} else {
	echo $output_string;		
}

?>
