<?php /* Single item displayed in loop: blog */ ?>

<?php
/* Get term + icon */
$terms = wp_get_post_terms(get_the_ID(),'subject');
foreach ($terms as $term) {
	$post_icon = get_field('subject-icon','subject_'.$term->term_id);
	if ($post_icon) {
		$post_icon = file_get_contents($post_icon['url']);//bstcm_load_svg(basename($post_icon['url']),false,false);
		$post_icons .= $post_icon;
	}
}
?>

<div class="content__filter__item">
	<a href="<?php the_permalink(); ?>">
				
		<div class="content__filter__item__metatop">
			<?php if (get_field('post-author')) { $author = get_field('post-author'); } else { $author = get_the_author(); } ?>
			<span class="content__filter__item__metatop--category">Blog &nbsp;&bull; </span><span class="content__filter__item__metatop--date"><?php echo get_the_date(); ?> &nbsp;&bull; </span><span class="content__filter__item__metatop--date"><?php echo $author; ?> &nbsp;&nbsp; </span><div class="content__filter__item__icons"><?php echo $post_icons; ?></div>
		</div>
		<h2 class="h2--large"><?php the_title(); ?></h2>
		<?php the_excerpt(); ?>
		<div class="content__filter__item__metabottom">
			<?php
			$categories = wp_get_post_terms($post->ID,'category');
			foreach ($categories as $category) {
				if ($category->term_id != 2) {
					if ($category->parent) { 
						$category = get_term_by('term_id',$category->parent,'category');
						break;
					} else {
						$category = $category;
						break;
					}
				}
			}
			$date = get_the_date();
			$locations = "";
			while (has_sub_field('post-locations')) {
				$details = get_sub_field('post-location');
				$locations .= $details['address'];
			}
			if ($locations) { 
				$location = "&bull; ".$locations;
			}
			?>
			<?php echo $category->name; ?> &bull; <?php echo $date; ?> <?php echo $location; ?>
		</div>
	
	</a>
	
</div>