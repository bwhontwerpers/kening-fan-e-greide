<?php /* Small google maps for single posts */ global $lat,$lng,$title,$locations,$post_icon_url; ?>
		<div class="sidebar__wrapper__map">
			<div class="content__filter__item__date">Locations</div>
			<div class="sidebar__wrapper__mapobject" data-icon="<?php echo $post_icon_url; ?>" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>" data-title="<?php echo $title; ?>"></div>
		</div>