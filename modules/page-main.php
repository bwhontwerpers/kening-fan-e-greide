<?php /* Template name: Hoofdpagina */ get_header(); ?>

<?php while (have_posts()) { the_post(); ?>

	<?php $backdrop = get_field('header-image'); ?>
	<div class="row content-header" style="background-image: url('<?php echo $backdrop['sizes']['page-header-visual']; ?>');">
		<div class="content-header__overlay"></div>
		<h1 class="h1 content-header__title"><?php the_title(); ?></h1>
		<ul class="content-header__menu">
		<?php 
		$subpagesQuery = new WP_Query(array('post_type'=>'page','post_parent'=>get_the_ID()));
		while ($subpagesQuery->have_posts()) { 
			$subpagesQuery->the_post();
			?>
			<li class="menu-item"><a href="#<?php echo $post->post_name; ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
			<?php
		} wp_reset_query();
		?>
		</ul>
	</div>
	
	<div class="row content-subpage content-subpage__intro">
		<div class="section">
			<div class="col col_16">
				<?php the_field('page-intro'); ?>
			</div>
		</div>	
	</div>
	
	<?php
	$subpagesQuery = new WP_Query(array('post_type'=>'page','post_parent'=>get_the_ID()));
	while ($subpagesQuery->have_posts()) { 
		$subpagesQuery->the_post();
		$subpageTemplate = str_replace(".php","",get_page_template_slug(get_the_ID()));
		get_template_part("modules/subpage",$subpageTemplate);
	} wp_reset_query();
	?>
	
<?php } ?>
	
<?php get_footer(); ?>