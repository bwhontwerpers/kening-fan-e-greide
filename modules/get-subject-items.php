<?php
if ($_GET['filter']) { $customfilter = $_GET['filter']; }
if (!$customfilter) { $customfilter = $_POST['category']; }
if (!$_POST['offset']) { $customoffset = 0; } else { $customoffset = $_POST['offset']; }

if (bwh_get_region_session()) {
	$taxQuery = array(
					'relation'=>'AND',
			        array(
			            'taxonomy' => 'regio',
			            'field' => 'slug',
			            'terms' => array(bwh_get_region_session()),
			            'operator' => 'IN',
			        ),
				    array(
			            'taxonomy' => 'category',
			            'field' => 'slug',
			            'terms' => array($customfilter),
			        )
			    );	    
			    
	/* Run WP query to get items */
	$itemsQuery = new WP_Query(
		array('post_type'=>'post',
			  'posts_per_page'=>14,
			  'tax_query'=>$taxQuery,
			  'no_found_rows' => true,
			  'update_post_term_cache' => false,
			  'update_post_meta_cache' => false,
			  'orderby'=>'date',
			  'order'=>'DESC',
			  'offset'=>$customoffset,
		)
	);
	
	/*	
	$itemsQuery = new WP_Query(
		array('post_type'=>'post',
			  'tax_query'=>$taxQuery,
			  'posts_per_page'=>14,
			  'post_status' => 'publish',
			  'orderby'=>'date',
			  'order'=>'DESC',
			  'offset'=>$customoffset,
		)
	);
	*/

} else {
	$taxQuery = array();
	
	/* Run WP query to get items */
	$itemsQuery = new WP_Query(
		array('post_type'=>'post',
			  'posts_per_page'=>14,
			  'category_name'=>$customfilter,
			  'no_found_rows' => true,
			  'update_post_term_cache' => false,
			  'update_post_meta_cache' => false,
			  'orderby'=>'date',
			  'order'=>'DESC',
			  'offset'=>$customoffset,
		)
	);

}



while ($itemsQuery->have_posts()) { 
	$itemsQuery->the_post(); 
	$itemTerms = wp_get_post_terms(get_the_ID(),'category');
	foreach ($itemTerms as $itemTerm) { if ($itemTerm->term_id != 2) { break; } }
	echo "<div class='filter__item__wrapper filter__item__wrapper--".$itemTerm->slug."'>";
	get_template_part("modules/loop","post-{$itemTerm->slug}");
	echo "</div>";
}
?>
