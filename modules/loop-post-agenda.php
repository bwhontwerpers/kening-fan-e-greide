<?php /* Single item displayed in loop: agenda */ ?>

<?php if (strtotime(get_field('post-date')) < time()) { $date_class = "history"; } ?>

<div class="content__filter__item content__filter__item--agenda <?php echo $date_class; ?>">
	
	<a href="<?php the_permalink(); ?>">
		
		<div class="content__filter__wrapper">
			<div class="content__filter__item__date"><?php echo date_i18n('d F Y',strtotime(get_field('post-date'))); ?></div>

			<div class="content__filter__item__meta content__filter__item__meta--small">
				<?php
				$locations = "";
				while (has_sub_field('post-locations')) {
					$details = get_sub_field('post-location');
					$locations .= $details['address'];
				}
				if ($locations) { 
					$location = "Locatie: ".$locations;
				}
				?>
				<?php echo $location; ?>
				<?php if (get_field('post-entree')) { echo "<br/>Entree: &euro; "; the_field('post-entree'); } ?>
			</div>
	
			
			<h2 class="h2--large"><?php the_title(); ?></h2>
			<?php the_excerpt(); ?>
		
		</div>
		
	</a>
</div>