<?php /* Header frontpage */ ?>


		
		
		<div class="header__top">

				<div class="section">
					
					<div class="col col_11">
						<a href="/" class="header__top__link"><?php bstcm_load_svg('kfeg_logo_svg.svg', 'header__visual__logo', true); ?></a>
						<?php bstcm_load_svg('kfeg_picto_egg.svg', 'header__visual__icon animated', true); ?>
						<?php bstcm_load_svg('kfeg_picto_close.svg', 'header__visual__close animated', true); ?>
						<a href="/" class="header__top__link"><img src="/wp-content/themes/keningfanegreide/lib/img/kfeg_menu_logo.svg" class="header__visual__textlogo animated" /></a>
					</div>
				
					<div class="col col_5">
						<?php $logos = get_field('global-celebrating-logos','option'); shuffle($logos); $logo = array_shift($logos); ?>
						<img src="<?php echo $logo['url']; ?>" class="header__visual__brand animated" />
					</div>
	
				</div>
				
		</div>
		
		<div class="header__mobilenav animated_drawer" data-height-offset="190">
			
			<div class="col col_16 col_first">
				<svg version="1.1" id="Laag_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" class="header__visual__close header__visual__close--mobile animated" 
					 viewBox="-30 30.7 38.2 47.8" style="enable-background:new -30 30.7 38.2 47.8;" xml:space="preserve" width="38.2" height="47.8">
				<path d="M-4.3,35.3c0,0-3.1-4.4-6.8-4.4h-0.1c-3.7,0.1-6.8,4.4-6.8,4.4S-28,48.4-28,60.2s10.3,18.3,16.7,18.3h0.1
					c6.5,0,16.7-6.4,16.7-18.3S-4.3,35.3-4.3,35.3z M-2.2,64.7c-0.2,0.3-0.5,0.5-0.9,0.5c-1-0.1-1.8-0.5-2.3-1.3
					c-0.9-1.3-1.9-2.5-2.9-3.7c-0.5-0.5-1-1.1-1.6-1.5l-0.3-0.3c-0.3-0.3-0.5-0.3-0.8,0c-1,1-2,2-3.3,3.4c-0.4,0.5-0.8,0.9-1.3,1.4
					c-0.5,0.6-1.1,1.2-1.6,1.7c-0.2,0.2-0.5,0.4-0.7,0.5c-0.1,0-0.1,0.1-0.2,0.1l-0.2,0.1l-0.2,0c-0.1,0-0.1,0-0.2,0
					c-0.1,0-0.2,0-0.4-0.1c-0.5-0.3-0.8-0.9-0.9-1.5c-0.1-0.6,0-1.1,0.3-1.5l0.3-0.4c1.5-1.9,3-3.8,4.8-5.4c0.3-0.2,0.6-0.5,0.8-0.8
					c0,0,0-0.2,0-0.2c-0.2-0.3-0.4-0.6-0.7-0.9c-0.5-0.4-1-0.9-1.4-1.3c-0.6-0.5-1.2-1-1.7-1.5c-0.6-0.6-1.2-1.2-1.8-2
					c-0.3-0.3-0.4-0.7-0.3-1.1c0.1-0.4,0.3-0.7,0.6-0.9c0.6-0.4,1.3-0.3,1.8,0.3c0.3,0.3,0.6,0.7,0.9,1c0.6,0.6,1.2,1.3,1.8,1.9
					c0.8,0.8,1.8,1.6,2.8,2.4c0.4,0.3,0.6,0.3,1.1,0c0.3-0.2,0.6-0.5,0.9-0.8c0.1-0.1,0.3-0.3,0.4-0.4l1-0.8c1.1-0.9,2.2-1.8,3.3-2.8
					c0.2-0.2,0.5-0.4,0.9-0.5c0.3-0.1,0.8,0,1.1,0.3c0.2,0.3,0.4,0.8,0.2,1.2c-0.2,0.3-0.4,0.8-0.9,1.2c-0.4,0.4-0.8,0.7-1.3,1.1
					c-0.5,0.4-0.9,0.7-1.4,1.1c-1,0.9-1.9,1.9-2.6,2.6c-0.1,0.1-0.2,0.2-0.2,0.3c0,0.1,0.1,0.3,0.3,0.4c2.3,2,4.3,4.3,5.7,6
					c0.3,0.4,0.5,0.9,0.7,1.3l0.1,0.1C-1.9,64.1-2,64.4-2.2,64.7z"/>
				</svg>
				<a href="/"><img src="/wp-content/themes/keningfanegreide/lib/img/kfeg_menu_logo.svg" class="header__visual__textlogo animated" /></a>
			</div>
				
			<div class="col col_8 col_first header__visual__navitems">
			<?php 
			$menuTerms = get_terms('subject', array('hide_empty'=>false,'hierarchical'=>true) ); 
			foreach ($menuTerms as $menuTerm) {
				if ($menuTerm->parent != 0) { $class="sub"; } else { $class=""; } 
				if ($menuTerm->slug == get_query_var('subject')) { $class .= " current-menu-item"; }
				$picto = get_field('subject-icon','subject_'.$menuTerm->term_id);
				?>
				<li class="header__visual__navitem <?php echo $class; ?>">
					<a href="<?php echo get_term_link($menuTerm); ?>" title="<?php echo $menuTerm->name; ?>">
						<img src="<?php echo $picto['url']; ?>" />
						<span><?php echo $menuTerm->name; ?></span>
					</a>
				</li>
				<?php 
			}
			?>
			</div>
				
			<div class="col col_8">				
				<div class="header__visual__pageitems">
					<a href="http://volg.keningfanegreide.nl" target="_blank" title="Volg grutto's" class="header__visual__pageitems__item">Volg grutto's</a>
					<?php
					if (bwh_get_region_session()) { $session = bwh_get_region_session(); } else { $session = "nederland"; }
					$aboutPage = array_shift(get_posts(array('post_type'=>'page','post_parent'=>0,'tax_query'=>array(array('taxonomy'=>'regio','field'=>'slug','terms'=>array($session))))));
					?>
					<a href="<?php echo get_permalink($aboutPage->ID); ?>" title="Over Kening Fan 'e Greide" class="header__visual__pageitems__item"><?php echo get_the_title($aboutPage->ID); ?></a>
				</div>
			</div>
			
			<div class="col col_16 content__sidebar__region__wrapper--mobile">
				<form method="post" action="./" name="regionform" onchange="this.submit();">
					<label for="region" class="content__sidebar__region content__sidebar__region--mobile">
						<select name="region">
							<?php if ( 'nederland' == $session || 'fryslan' == $session ) { ?><option value="-">Alle regio's</option><?php } else { ?><option value="-">All regions</option><?php } ?>
							<?php $regios = get_terms(array('regio'),array('hide_empty'=>true)); foreach ($regios as $regio) { ?>
								<?php if ($session && $regio->slug == $session) { ?>
									<option value="<?php echo $regio->slug; ?>" selected="selected"><?php echo $regio->name; ?></option>
								<?php } else { ?>
									<option value="<?php echo $regio->slug; ?>"><?php echo $regio->name; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</label>
				</form>
			</div>

		</div>
		
		<div class="header__nav" data-height-offset="0">
				
			<div class="section">
				
				<div class="col col_16 col_first header__visual__navitems">
				<?php 
				$menuTerms = get_terms('subject', array('hide_empty'=>false,'hierarchical'=>true) ); 
				foreach ($menuTerms as $menuTerm) {
					if ($menuTerm->parent != 0) { $class="sub"; } else { $class=""; } 
					if ($menuTerm->slug == get_query_var('subject')) { $class .= " current-menu-item"; }
					$subtitle = get_field('subject-subtitle','subject_'.$menuTerm->term_id);
					$picto = get_field('subject-icon','subject_'.$menuTerm->term_id);
					?>
					<li class="header__visual__navitem <?php echo $class; ?>">
						<a href="<?php echo get_term_link($menuTerm); ?>" title="<?php echo $menuTerm->name; ?>" data-subtitle="<?php echo $subtitle; ?>">
							<img src="<?php echo $picto['url']; ?>" />
							<span><?php echo $menuTerm->name; ?></span>
						</a>
					</li>
					<?php 
				}
				?>
				</div>
			
			</div>				
				
			
			<div class="header__visual__pageitems">
				<a href="http://volg.keningfanegreide.nl" target="_blank" title="Volg grutto's" class="header__visual__pageitems__item">Volg grutto's</a>
					<?php
					if (bwh_get_region_session()) { $session = bwh_get_region_session(); } else { $session = "nederland"; }
					$aboutPage = array_shift(get_posts(array('post_type'=>'page','post_parent'=>0,'tax_query'=>array(array('taxonomy'=>'regio','field'=>'slug','terms'=>array($session))))));
					?>
					<a href="<?php echo get_permalink($aboutPage->ID); ?>" title="Over Kening Fan 'e Greide" class="header__visual__pageitems__item"><?php echo get_the_title($aboutPage->ID); ?></a>
			</div>
			
				<div class="header__visual__extraitems">
					<div class="">
						<a href="#" title="Search" class="content__sidebar__button" rel="init_search_module"><img src="/wp-content/themes/keningfanegreide/lib/img/kfeg_picto-search.svg" /> <span class="animated">Zoeken</span></a>
						<?php if ("nederland" == $session) { ?>
							<a href="#" title="Region" class="content__sidebar__button" rel="init_region_module">
						<?php } else { ?>
							<a href="#" title="Region" class="content__sidebar__button" rel="init_region_module">
						<?php } ?>
							
							<?php if ("nederland" == $session) { ?>
								<img class="world bounce" src="/wp-content/themes/keningfanegreide/lib/img/kfeg_picto-world.svg" />
								<img class="lang bounce" src="/wp-content/themes/keningfanegreide/lib/img/kfeg_picto-lang.svg" /> <span class="animated">Fryslân</span>
								<script>
									jQuery(document).ready(function() {
										setTimeout(function() { 
											jQuery('.content__sidebar__button').addClass('visible');
											jQuery('img.lang').addClass('visible'); 
										}, 3000);
										setTimeout(function() { 
											jQuery('.content__sidebar__button').addClass('showlabel');
										}, 3275);
									});
								</script>
							<?php } else { ?>
								<img class="marker visible bounce" src="/wp-content/themes/keningfanegreide/lib/img/kfeg_picto-worldmarker.svg" /> <span class="animated">Fryslân</span>
							<?php } ?>
						</a>
						<?php if (bwh_get_region_session()) { $session = bwh_get_region_session(); } ?>
						<form method="get" action="/" name="search"><input type="search" name="s" value="" placeholder="<?php if ( 'nederland' == $session || 'fryslan' == $session ) { ?>Zoeken naar...<?php } else { ?>Search for...<?php } ?>" class="content__sidebar__search" /></form>
						
						<form method="post" action="./" name="regionform" onchange="this.submit();">
							<label for="region" class="content__sidebar__region">
								<select name="region">
									<?php if ( 'nederland' == $session || 'fryslan' == $session ) { ?><option value="-">Alle regio's</option><?php } else { ?><option value="-">All regions</option><?php } ?>
									<?php $regios = get_terms(array('regio'),array('hide_empty'=>true)); foreach ($regios as $regio) { ?>
										<?php if ($session && $regio->slug == $session) { ?>
											<option value="<?php echo $regio->slug; ?>" selected="selected"><?php echo $regio->name; ?></option>
										<?php } else { ?>
											<option value="<?php echo $regio->slug; ?>"><?php echo $regio->name; ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</label>
						</form>
					</div>
				</div>

			<div class="header__nav__underlay animated">&nbsp;</div>
		</div>
		
		<div class="row header" data-height-offset="190">
			
			<?php $visual = get_field('global-visual-image','option'); ?>
			<div class="header__visual" style="background-image:url('<?php echo $visual['sizes']['header-backdrop-visual']; ?>');"></div>
			
			<div class="section">
				<div class="col col_4" style="display:none;">
					<div class="header__visual__pageitems">
						<?php wp_nav_menu(array('menu'=>27)); ?>
					</div>
				</div>
				
				<div class="col col_12">&nbsp;</div>
			</div>
			
			<div class="header__visual__interaction animated">
				<h3><?php the_field('global-title-small','option'); ?></h3>
				<h2><?php the_field('global-title-large','option'); ?></h2>
				<a class="header__visual__interaction__button" href="<?php the_field('global-button-link','option'); ?>" title="<?php the_field('global-button-label','option'); ?>">
					<?php the_field('global-button-label','option'); ?> <?php bstcm_load_svg('kfeg_arrow_right.svg',false); ?>
				</a>
			</div>
			
		</div>