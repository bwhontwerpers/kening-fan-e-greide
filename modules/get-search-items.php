<?php
if (!$_POST['offset']) { $customoffset = 0; } else { $customoffset = $_POST['offset']; }

/*
$itemsQuery = new WP_Query(
	array('post_type'=>'post',
		  'posts_per_page'=>12,
		  'subject'=>get_query_var('subject'),
		  'category_name'=>$customfilter,
		  'orderby'=>'date',
		  'order'=>'DESC',
		  'offset'=>$customoffset,
	)
);
*/

while (have_posts()) { 
	the_post(); 
	$itemTerms = wp_get_post_terms(get_the_ID(),'category');
	foreach ($itemTerms as $itemTerm) { 
		if ($itemTerm->term_id != 2) { break; }
	}
	
	echo "<div class='filter__item__wrapper filter__item__wrapper--".$itemTerm->slug."'>";
	get_template_part("modules/loop","post-{$itemTerm->slug}");
	echo "</div>";
	
}
?>
