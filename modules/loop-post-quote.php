<?php /* Single item displayed in loop: quote */ ?>

<div class="content__filter__item content__filter__item--quote">
	<blockquote class="content__filter__item__quote"><?php the_field('post-quote-text'); ?></blockquote>
	<div class="content__filter__item__meta"><?php the_field('post-quote-author'); ?></div>
</div>