<?php /* Single item displayed in loop: projecten */ ?>

<div class="content__filter__item content__filter__item--project">
	
	<a href="<?php the_permalink(); ?>">
	
		<div class="content__filter__item__projectthumbnail">
			<?php if (has_post_thumbnail()) { ?>
				<?php the_post_thumbnail('project-loop-thumbnail'); ?>
			<?php } ?>
			<h2><?php the_title(); ?></h2>
			<div class="content__filter__backdrop"></div>
		</div>
			
	</a>
	
</div>