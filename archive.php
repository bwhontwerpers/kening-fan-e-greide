<?php 
get_header();
if (!$_GET['filter']) { $_GET['filter'] = "uitgelicht"; }
$customfilter = isset($_GET['filter']) ? $_GET['filter'] : 'uitgelicht';
$subject = get_term_by('slug',get_query_var('subject'),'subject');
global $totalposts;
$itemsQuery = new WP_Query(
	array('post_type' => 'post',
		  'fields' => 'ids',
		  'posts_per_page'=> -1,
		  'subject' => get_query_var('subject'),
		  'no_found_rows' => true,
		  'update_post_term_cache' => false,
		  'update_post_meta_cache' => false,
		  'category_name'=>$customfilter,
	)
);
$totalposts = $itemsQuery->post_count;
?>
		
	<a href="javascript:scroll_content_to_top();" class="content__wrapper__top__wrapper">
		<div class="content__wrapper__top"><?php bstcm_load_svg('kfeg_arrow_top.svg',false,true); ?></div>
		<div class="content__wrapper__top__text"><strong><?php echo $subject->name; ?></strong> &rsaquo; <?php echo $customfilter; ?></div>
	</a>
	
	<div class="row content">
		
		<div class="section">
			<div class="col col_16 col_first dynamic_width">
				<div class="content__wrapper">
					<div class="site_overlay"></div>
					<h1 class="h1--archive"><?php single_term_title(false,true); ?></h1>
					<?php if (get_field('subject-subtitle','subject_'.$subject->term_id)) { ?><h3 class="h3--subtitle"><?php the_field('subject-subtitle','subject_'.$subject->term_id); ?></h3><?php } ?>
										
					<ul class="content__filter__types">
						<?php 
						$menuTypes = get_terms('category', array('hide_empty'=>true,'hierarchical'=>false,'parent'=>0) ); 
						foreach ($menuTypes as $menuType) { 
							if ($customfilter==$menuType->slug) { $class="active"; } else { $class = ""; }
							?>
							<li class="content__filter__type <?php echo $class; ?>"><a href="./?filter=<?php echo $menuType->slug; ?>" title="<?php echo $menuType->name; ?>"><?php echo $menuType->name; ?></a></li>
							<?php 
						}
						?>
					</ul>
					
					<div class="content__filter__items" data-columns data-custom-filter="<?php echo $customfilter; ?>">
						<?php get_template_part("modules/get","subject-items"); ?>
					</div>
					
					<div class="col col_16 col_first content--styled">
						<?php if ($totalposts>12) { ?>
							<a href="javascript:load_filter_items();" id="load_more_items" class="content__filter__loadmore" data-total="<?php echo $totalposts; ?>" data-offset="12" data-limit="12" data-category="<?php echo $customfilter; ?>">Meer laden</a>
						<?php } ?>
					</div>
					
				</div>
				
			</div>
		</div>
		
	</div>
	
<?php get_footer(); ?>