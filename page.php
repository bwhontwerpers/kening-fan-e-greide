<?php 
/* Standaard pagina */
get_header();
if ($post->post_parent == 0) { $page_parent = $post->ID; } else { $page_parent = $post->post_parent; }
?>

	<?php while(have_posts()) { the_post(); ?>
	
	<a href="javascript:window.history.go(-1);" class="content__wrapper__back">
		<?php bstcm_load_svg('kfeg_arrow_left.svg',false,true); ?>
	</a>
				
	<div class="row content dynamic_width">
		<div class="section">
			
			<div class="col col_16 col_first">
				<div class="content__wrapper">
					<div class="site_overlay"></div>
										
					<div class="col col_3">&nbsp;</div>
					<div class="col col_10 col_first content__wrapper__article__header">
						<h1 class="h1--single h1--page"><?php the_title(); ?></h1>
						
						<?php if (get_field('subject-subtitle','subject_'.$subject->term_id)) { ?><h3 class="h3--subtitle"><?php the_field('subject-subtitle','subject_'.$subject->term_id); ?></h3><?php } ?>									
						<ul class="content__filter__types content__filter__types--subpages">
							<?php
							$menuPages = get_pages(array('parent'=>$page_parent,'sort_column'=>'menu_order'));
							foreach ($menuPages as $menuPage) {
								if ($menuPage->ID == $post->ID) { $class="active"; } else { $class=""; }
								?>
								<li class="content__filter__type <?php echo $class; ?>"><a href="<?php echo get_permalink($menuPage->ID); ?>" title="<?php echo get_the_title($menuPage->ID); ?>"><?php echo get_the_title($menuPage->ID); ?></a></li>
								<?php
							}
							?>
						</ul>						
					</div>
					<div class="col col_3">&nbsp;</div>
					
					<div class="clearfix"></div>
					
					<?php if (has_post_thumbnail() && get_field('slideshow')) { ?>
						<div class="col col_2">&nbsp;</div>
						<div class="col col_12 col_first content__wrapper__postertop">
							<div class="content__slideshow">
							<?php the_post_thumbnail('content-slideshow-slide'); ?>
							<?php $gallery = get_field('slideshow'); foreach ($gallery as $slide) { ?>
								<div class="content__slide">
									<img src="<?php echo $slide['sizes']['content-slideshow-slide']; ?>"/>
								</div>
							<?php } ?>
							</div>
						</div>
						<div class="col col_2">&nbsp;</div>

					<?php } elseif( !has_post_thumbnail() && get_field('slideshow')) { ?>
						<div class="col col_2">&nbsp;</div>
						<div class="col col_12 col_first content__wrapper__postertop">
							<div class="content__slideshow">
							<?php $gallery = get_field('slideshow'); foreach ($gallery as $slide) { ?>
								<div class="content__slide">
									<img src="<?php echo $slide['sizes']['content-slideshow-slide']; ?>"/>
								</div>
							<?php } ?>
							</div>
						</div>
						<div class="col col_2">&nbsp;</div>

					<?php } elseif (has_post_thumbnail() && !get_field('slideshow')) { ?>						
						<div class="col col_2">&nbsp;</div>
						<div class="col col_12 col_first content__wrapper__postertop">				
							<div class="content__slideshow">
								<div class="content__slide">
									<?php the_post_thumbnail('content-slideshow-slide'); ?>
								</div>
							</div>
						</div>
						<div class="col col_2">&nbsp;</div>

					<?php } ?>
					
					<div class="clearfix"></div>
										
					<div class="col col_3">&nbsp;</div>
					<div class="col col_10 col_first content--styled">
						<?php the_content(); ?>
						
						<?php if (get_field('links')) { ?>
							<div class="content__wrapper__attachments">
								<?php while (has_sub_field('links')) { $file = get_sub_field('link_file'); ?>
									<a href="<?php echo $file['url']; ?>" title="<?php echo $file['title']; ?>" class="content__wrapper__attachment-item" target="_blank"><?php echo $file['title']; ?></a>
								<?php } ?>
							</div>
						<?php } ?>

					</div>
					<div class="col col_3">&nbsp;</div>

					<div class="clearfix"></div>					
					
				</div>
			</div>
			
			<?php if (get_field('related')) { ?>
			<div class="col col_16 col_first related__column">
				<div class="related__wrapper">
					<div class="col col_2 col_first">&nbsp;</div>
					<div class="col col_12 col_first">
					<h2 class="h2--related--header">Gerelateerd aan dit artikel</h2>
					<?php 
					$related = get_field('related'); 
					foreach ($related as $relatedArticle) { 
						$post_terms = wp_get_post_terms(get_the_ID(),'subject');
						$post_icons = "";
						foreach ($post_terms as $post_term) {
							$post_icon = get_field('subject-icon','subject_'.$post_term->term_id);
							if ($post_icon) {
								$post_icon = file_get_contents($post_icon['url']);//bstcm_load_svg(basename($post_icon['url']),false,false);
								$post_icons .= $post_icon;
							}
						}
						$post_types = wp_get_post_terms($relatedArticle->ID,'category');
						$post_types_text = "";
						foreach ($post_types as $post_type) {
							if ($post_type->term_id != 2) {
								$post_types_text = $post_type->name;
								break;
							}
						}
						?>
						<div class="col col_third related__item">
							<a href="<?php echo get_permalink($relatedArticle->ID); ?>">
							<?php if (has_post_thumbnail($relatedArticle->ID)) { ?>
								<?php echo get_the_post_thumbnail($relatedArticle->ID,'single-sidebar-thumbnail'); ?>
							<?php } ?>
							<h2 class="h2--related"><?php echo get_the_title($relatedArticle->ID); ?></h2>
							<div class="content__filter__item__metatop content__filter__item__metatop--small">
								<span class="content__filter__item__metatop--category"><?php echo $post_types_text; ?> &nbsp;&bull; </span><span class="content__filter__item__metatop--date"><?php echo date_i18n('d-m-Y',strtotime($relatedArticle->post_date)); ?> &nbsp;&nbsp; </span><div class="content__filter__item__icons"><?php echo $post_icons; ?></div>
							</div>
							<!--<div class="content__filter__item__excerpt"><?php the_excerpt(); ?></div>-->
							</a>
						</div>
					<?php } ?>
					</div>
					<div class="col col_2 col_first">&nbsp;</div>
				</div>
			</div>
			<?php } ?>
			
			<div class="col col_16 col_first morenews__column">
				<div class="morenews__wrapper">
					<div class="col col_2 col_first">&nbsp;</div>
					<div class="col col_12 col_first">
					<?php 
					$articlesQuery = new WP_Query(array('post_type'=>'post','posts_per_page'=>3,'tax_query'=>array(array('taxonomy'=>'subject','field'=>'term_id','terms'=>array($other_news_term->term_id))),'category_name'=>'nieuws'));
					while ($articlesQuery->have_posts()) {
						$articlesQuery->the_post();
						$post_terms = wp_get_post_terms(get_the_ID(),'subject');
						$post_icons = "";
						foreach ($post_terms as $post_term) {
							$post_icon = get_field('subject-icon','subject_'.$post_term->term_id);
							if ($post_icon) {
								$post_icon = file_get_contents($post_icon['url']);
								$post_icons .= $post_icon;
							}
						}
						$post_types = wp_get_post_terms($post->ID,'category');
						$post_types_text = "";
						foreach ($post_types as $post_type) {
							if ($post_type->term_id != 2) {
								$post_types_text = $post_type->name;
								break;
							}
						}
						?>
						<div class="col col_third related__item">
							<a href="<?php echo get_permalink($post->ID); ?>">
							<?php if (has_post_thumbnail($post->ID)) { ?>
								<?php echo get_the_post_thumbnail($post->ID,'single-sidebar-thumbnail'); ?>
							<?php } ?>
							<h2 class="h2--related"><?php echo get_the_title($post->ID); ?></h2>
							<div class="content__filter__item__metatop content__filter__item__metatop--small">
								<span class="content__filter__item__metatop--category"><?php echo $post_types_text; ?> &nbsp;&nbsp;&bull;</span><span class="content__filter__item__metatop--date"><?php echo date_i18n('d-m-Y',strtotime($relatedArticle->post_date)); ?> &nbsp;&nbsp; </span><div class="content__filter__item__icons"><?php echo $post_icons; ?></div>
							</div>
							<!--<div class="content__filter__item__excerpt"><?php the_excerpt(); ?></div>-->
							</a>
						</div>
					<?php } wp_reset_query(); ?>
					</div>
					<div class="col col_2 col_first">&nbsp;</div>
				</div>
			</div>

			
		</div>
	</div>
	
	<?php } ?>
	
<?php get_footer(); ?>