<?php 
/* Template name: Tracker pagina */
get_header();
?>

	<?php while(have_posts()) { the_post(); ?>
	
	<div class="row content dynamic_width content--tracker">		
		<div class="section">
			
			<div class="col col_16 col_first">
				<div class="content__wrapper">
					<!--<div class="col col_16 col_first content--styled"><h1 class="h1--single" style="text-align: center;"><?php the_title(); ?> </h1></div>-->
					<div class="col col_16 col_first content--styled"><?php the_content(); ?></div>
				</div>
			</div>
			
		</div>
	</div>
	
	<?php } ?>
	
<?php get_footer(); ?>