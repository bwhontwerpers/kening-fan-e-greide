<?php header('X-UA-Compatible: IE=edge,chrome=1'); setlocale(LC_ALL, 'nl_NL'); ?><?php if ($_POST['region']) { bwh_set_region_session($_POST['region']); } ?><!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9) ]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php wp_head(); ?>
	
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','__gaTracker');
	
		__gaTracker('create', 'UA-56931585-1', 'auto');
		//__gaTracker('set', 'forceSSL', true);
		__gaTracker('send','pageview');
	
	</script>
</head>

<body <?php body_class(); ?>>

	<!-- Logo -->
	<div class="start_logo">
		<img src="<?php echo site_url(); ?>/pres/img/logo.svg" width="167" height="125" />
	</div>
	
	<!-- Navigation -->
	<div class="start_nav fade">
		<span id="p"><</span>
		<!-- <p id="hst"></p> -->
		<span id="n">></span>
		<div class="clear"> </div>
	</div>
	
	<div class="site__presentation">
		<div class="loader">
		</div>
		<div class="inner"></div>
	</div>

	<?php 
	if (is_front_page()) {
		get_template_part("modules/header","front");
	} else {
		get_template_part("modules/header","default");
	}
	?>

	<div class="site animated_drawer">
		