<?php
get_header();

/* Get post term icons */
$terms = wp_get_post_terms(get_the_ID(),'subject');
foreach ($terms as $term) {
	$post_icon = get_field('subject-icon','subject_'.$term->term_id);
	if ($post_icon) {
		$post_icon_url = $post_icon['url'];
		$post_icon = file_get_contents($post_icon['url']);//bstcm_load_svg(basename($post_icon['url']),false,false);
		$post_icons .= $post_icon;
	}
}
$current_id = get_the_ID();

/* Get post type text */
$post_types = wp_get_post_terms(get_the_ID(),'category');
$post_types_text = "";
foreach ($post_types as $post_type) {
	if ($post_type->term_id != 2) {
		$post_types_text = $post_type->name;
		break;
	}
}

/* Get post locations */
$locations = get_field('post-locations');
?>

	<a href="javascript:window.history.go(-1);" class="content__wrapper__back">
		<?php bstcm_load_svg('kfeg_arrow_left.svg',false,true); ?>
	</a>
	
	<div class="row content project dynamic_width">		
		<div class="section">
			
			<div class="col col_16 col_first">
				
				<?php if (get_field('post-locations')) { ?>
											<div class="content__switch__view" onclick="toggle_jumbo_map();">
								<svg version="1.1" id="mapicon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="-36 37 25.5 27" style="enable-background:new -36 37 25.5 27;" xml:space="preserve" width="25.5" height="27">
								<style type="text/css">
									.st0{fill:#FFFFFF;}
								</style>
								<path class="st0" d="M-36,42.4c0-0.9,0-1.3,0-1.7c0-0.6,0.3-1.1,0.9-1.4c1-0.5,2-1.1,3.1-1.5c1.5-0.6,3.1-0.7,4.6,0.2
									c0.8,0.5,1.7,0.9,2.5,1.4c0.7,0.4,1.5,0.5,2.2,0.2c1.3-0.5,2.7-1,3.7-2c0.5-0.5,1.1-0.7,1.8-0.6c1.2,0.1,2.5,0.2,3.7,0.5
									c0.8,0.2,1.5,0.7,2.3,1.2c0.5,0.3,0.7,0.9,0.7,1.5c0,0.6,0,1.1-0.1,1.7c-0.5,3-0.3,6.1-0.1,9.1c0.1,1.3,0,2.5-0.2,3.7
									c-0.1,0.7-0.4,1-1.1,1c-0.4,0-0.8,0-1.1,0c-0.6,0-1.1,0.4-1.4,0.9c-1.1,2.2-2.2,4.4-3.2,6.7c-0.4,0.8-1.2,1-1.8,0.3
									c-0.2-0.2-0.4-0.4-0.5-0.7c-0.5-0.6-1-1.2-1.4-1.8c-1.1-1.9-2.7-3.2-4.6-4.1c-0.4-0.2-0.8-0.5-1.3-0.7c-0.8-0.4-1.6-0.5-2.4,0
									c-1.2,0.8-2.5,1.3-4,1.5c-0.9,0.1-1.5-0.3-1.5-1.2c-0.1-1.6-0.1-3.2-0.2-4.9c-0.1-2.7,0-5.4-0.5-8.2C-36,43-36,42.4-36,42.4z
									 M-11.8,47.2h-0.1c0-0.7,0-1.3,0-2c0-0.8,0.1-1.5,0.1-2.3c0-1,0-1.9-0.2-2.8c-0.1-0.4-0.4-0.9-0.7-1.1c-1-0.7-4.6-0.8-5.6-0.3
									c-0.1,0-0.2,0.1-0.2,0.2c-1,1.2-2.5,1.3-3.7,2c-0.9,0.5-1.8,0.3-2.7-0.2c-1-0.7-1.9-1.2-2.8-1.7c-1.2-0.7-2.6-0.7-3.8-0.2
									c-1,0.4-1.8,1-2.7,1.5c-0.3,0.3-0.6,0.7-0.6,1c-0.1,0.8,0,1.5,0.1,2.3c0.1,1,0.3,2.1,0.4,3.1c0.1,1.6,0,3.2,0.1,4.9
									c0,1.2,0.1,2.5,0.3,3.7c0.1,1.1,0.8,1.4,1.9,1c0.7-0.3,1.4-0.6,2-1.1c0.8-0.6,1.5-0.6,2.4-0.1c1,0.5,2.1,0.9,3.2,1.3
									c0.2,0.1,0.6-0.2,0.7-0.3c0.1-0.3,0.1-0.7,0.1-1.1c0.1-1,0-2.1,0.2-3.1c0.5-2.5,2.4-3.8,5-3.9c1.5-0.1,3.9,1.1,4.5,3.7
									c0.2,0.6,0.2,1.3,0.4,2c0.1,0.5,0.2,0.9,0.8,1c0.6,0.1,1-0.3,1.1-1.1c0-0.5,0.1-0.9,0.1-1.4C-11.8,50.4-11.8,48.8-11.8,47.2z
									 M-15.1,53.6c-0.1-0.7-0.2-1.5-0.4-2.2c-0.3-1-0.9-1.6-1.9-2c-1.9-0.8-4.3,0.3-4.8,2.4c-0.6,2.3-0.8,4.7,0.9,6.8
									c0.2,0.3,0.3,0.7,0.5,1c0.4,0.7,0.8,1.3,1.2,1.9c0.4,0.6,1,0.6,1.4,0c0.8-1.3,1.5-2.6,2.2-4C-15.5,56.2-15.1,55-15.1,53.6z"/>
								<path class="st0" d="M-20.8,53c0-0.2,0.1-0.6,0.2-1c0.5-1.4,2-1.7,3-0.5c0.5,0.6,0.9,1.3,0.6,2.1c-0.5,1.2-1.7,1.7-2.9,1.1
									C-20.6,54.4-20.8,53.8-20.8,53z M-18.1,53.3c0.1-0.6-0.2-1.1-0.7-1.3c-0.2-0.1-0.5,0-0.6,0.1c-0.5,0.5-0.5,1.2-0.2,1.7
									c0.2,0.3,0.6,0.4,0.8,0.2C-18.4,53.7-18.2,53.4-18.1,53.3z"/>
								<path class="st0" d="M-28.7,51c0,0.3,0,0.6,0,0.9c0,0.4-0.4,0.9-0.6,0.7c-0.2-0.1-0.5-0.3-0.6-0.5c-0.1-0.2-0.1-0.5-0.2-0.8
									c0-2.9,0-5.7-0.1-8.6c0-0.2,0-0.4,0.1-0.5c0.1-0.2,0.3-0.3,0.4-0.3c0.1,0,0.3,0.2,0.4,0.3c0.1,1.2,0.2,2.5,0.2,3.7
									C-29,47.6-28.9,49.3-28.7,51L-28.7,51z"/>
								<path class="st0" d="M-18.3,44.6c0-1.6,0-2.8,0.6-3.8c0.1-0.1,0.3-0.2,0.4-0.2s0.4,0.3,0.3,0.3c-0.5,1.5-0.3,3-0.2,4.5
									c0,0.4-0.1,0.7-0.2,1.1c0,0.1-0.3,0.2-0.4,0.2s-0.2-0.1-0.2-0.1C-18.1,45.8-18.2,44.9-18.3,44.6z"/>
								<path class="st0" d="M-23.9,46.3c0,0.8,0,1.4,0,2c0,0.1-0.2,0.3-0.4,0.3c-0.1,0-0.4-0.1-0.4-0.2c-0.3-1.5-0.3-3.1,0.4-4.6
									c0,0,0.2,0,0.3,0s0.2,0.1,0.2,0.1c0.1,0.2,0.1,0.3,0.1,0.5C-23.7,45.1-23.8,45.8-23.9,46.3z"/>
								</svg>
								<svg version="1.1" id="mapclose" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="-40 41.6 18.2 18.4" style="enable-background:new -40 41.6 18.2 18.4;" xml:space="preserve" width="18.2" height="18.4">
								<style type="text/css">
									.st0{fill:#FFFFFF;}
								</style>
								<path class="st0" d="M-38.8,60c-0.2-0.1-0.4-0.1-0.4-0.1c-0.7-0.5-1-1.8-0.5-2.5c1.7-2.2,3.4-4.4,5.5-6.2c0.4-0.3,0.7-0.6,1-1
									c0.1-0.2,0.2-0.6,0-0.8c-0.2-0.3-0.5-0.7-0.9-1c-1.1-1-2.3-2-3.4-3.1c-0.6-0.6-1.3-1.3-1.9-2.1c-0.4-0.5-0.3-1.1,0.2-1.4
									c0.4-0.3,0.9-0.2,1.3,0.2c0.9,1,1.9,2.1,2.9,3.1c0.9,0.9,2,1.8,3,2.6c0.6,0.5,1.1,0.5,1.8,0c0.5-0.4,1-0.8,1.5-1.3
									c1.5-1.3,3.1-2.6,4.6-3.8c0.2-0.2,0.5-0.4,0.7-0.4s0.6,0,0.7,0.2c0.2,0.2,0.3,0.6,0.2,0.8c-0.2,0.4-0.5,0.8-0.8,1.2
									c-0.9,0.8-1.9,1.5-2.8,2.4c-0.9,0.8-1.9,1.7-2.8,2.7c-0.5,0.5-0.4,1,0.2,1.5c2.3,1.9,4.2,4.1,6.1,6.4c0.3,0.4,0.6,1,0.8,1.5
									c0.2,0.3-0.2,0.8-0.6,0.8c-0.9-0.1-1.6-0.4-2.2-1.2c-1-1.4-2.1-2.7-3.2-4c-0.6-0.7-1.3-1.3-2-2c-0.5-0.5-1-0.5-1.5,0
									c-1.2,1.2-2.4,2.4-3.6,3.7c-1,1.1-2,2.3-3.1,3.3C-38.2,59.8-38.6,59.9-38.8,60z"/>
								</svg>
							</div>
				<?php } ?>
					
					
				
				<?php if (get_field('slideshow')) { $slides = get_field('slideshow'); $header = array_shift(get_field('slideshow')); ?>
					<div class="content__jumbo__slider__wrapper">
						<div class="site_overlay"></div>
						<?php if ( sizeof($slides)>1 ) { ?>
							<div class="content__jumbo__slider__left animated">&nbsp;</div>
							<div class="content__jumbo__slider__right animated">&nbsp;</div>
						<?php } ?>
						
						<?php
						if (get_field('post-locations')) {
							foreach ($locations as $location) {
								echo "<datalist id='{$post->post_name}' data-icon='".$post_icon_url."' data-title='".get_the_title()."' data-link='' data-lat='{$location['post-location']['lat']}' data-lng='{$location['post-location']['lng']}'></datalist> \n\r";
							}
							?>							

							<?php
						}
						?>
						
						<div class="content__jumbo__slider__caption">
							<h1 class="content__jumbo__slider__title"><?php the_title(); ?></h1>
							<h3 class="content__jumbo__slider__category">Project</h3>
						</div>
						<?php if (sizeof($slides)>1) { ?>
							<a href="<?php echo $header['sizes']['content-lightbox-gallery']; ?>" rel="lightbox[gallery-0]"><img src="<?php echo $header['sizes']['header-backdrop-visual']; ?>" /><!-- primaire link --></a>
						<?php } else { ?>
							<img src="<?php echo $header['sizes']['header-backdrop-visual']; ?>" />
						<?php } ?>
					</div>
					<?php if (sizeof($slides)>1) { array_shift($slides); foreach ($slides as $slide) { ?>
						<a href="<?php echo $slide['sizes']['content-lightbox-gallery']; ?>" rel="lightbox[gallery-0]" class="content__jumbo__slider__slide"><!-- verborgen link --></a>
					<?php } } ?>
				<?php } ?>
				
				<div class="content__wrapper content__wrapper--nopadding content__wrapper--nomargin">
					<div class="content__mapper content__mapper--project">&nbsp;</div>
					<div class="site_overlay"></div>
					<div class="col col_8 col_first content--styled content__wrapper--left">
						<?php the_content(); ?>
						
						<?php if (get_field('links')) { ?>
							<div class="content__wrapper__attachments">
								<?php while (has_sub_field('links')) { $file = get_sub_field('link_file'); ?>
									<a href="<?php echo $file['url']; ?>" title="<?php echo $file['title']; ?>" class="content__wrapper__attachment-item" target="_blank"><?php echo $file['title']; ?></a>
								<?php } ?>
							</div>
						<?php } ?>
						
						<div class="social__share">
							<a class="social__share__icon" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Delen op Facebook">
								<?php bstcm_load_image('kfeg_picto-facebook.svg',false,false,true); ?>
							</a>
							<?php $twitter_share_text = get_the_title()." - ".get_permalink(); ?>
							<a class="social__share__icon social__share__icon--twitter" target="_blank" href="https://twitter.com/home?status=<?php echo $twitter_share_text; ?>" title="Delen op Twitter">
								<?php bstcm_load_image('kfeg_picto-twitter.svg',false,false,true); ?>
							</a>
						</div>
						
					</div>
					
					<div class="col col_8 col_first content--styled content__wrapper--right">
												
						<?php if (get_field('goal')) { ?>
							<label class="content__wrapper__details-label">Doel</label>
							<span class="content__wrapper__details-info content__wrapper__details-info--jumbo"><?php the_field('goal'); ?></span>
						<?php } ?>

						<?php
						if (get_field('projectlogo')) {
							$logo = get_field('projectlogo');
							echo wp_get_attachment_image($logo['ID'],'fullsize',false,array('class'=>'content__wrapper__details-largelogo'));
						}
						?>
												
						<?php if (get_field('planning')) { ?>
							<label class="content__wrapper__details-label">Planning</label>
							<span class="content__wrapper__details-info"><?php the_field('planning'); ?></span>						
						<?php } ?>
						
						<?php if (get_field('website')) { ?>
							<label class="content__wrapper__details-label">Website</label>
							<span class="content__wrapper__details-info"><a href="http://<?php the_field('website'); ?>" target="_blank"><?php the_field('website'); ?></a></span>
						<?php } ?>
						
						<?php if (get_field('name_contactperson')) { ?>
							<label class="content__wrapper__details-label">Contact</label>
							<?php if (get_field('e-mail_contactperson')) { ?>
								<span class="content__wrapper__details-info"><a href="mailto:<?php the_field('e-mail_contactperson'); ?>"><?php the_field('name_contactperson'); ?></a></span>
							<?php } else { ?>
								<span class="content__wrapper__details-info"><?php the_field('name_contactperson'); ?></span>
							<?php } ?>
						<?php } ?>
						
						<?php if (get_field('participants')) { ?>
						<label class="content__wrapper__details-label">Participanten</label>
						<span class="content__wrapper__details-info"><?php the_field('participants'); ?></span>
						<?php } ?>
						
						<?php if (get_field('subsidieverstrekkers')) { ?>						
							<div class="content__wrapper__details-info">
							<?php while (has_sub_field('subsidieverstrekkers')) { $logo = get_sub_field('logo_supporter'); ?>
								<div class="content__wrapper__details-logo">
									<?php if (get_sub_field('site_supporter')) { ?>
										<a href="<?php the_sub_field('site_supporter'); ?>" target="_blank"><img src="<?php echo $logo['sizes']['content-sidebar-logo']; ?>" alt="Supporter logo" /></a>
									<?php } else { ?>
										<img src="<?php echo $logo['sizes']['content-sidebar-logo']; ?>" alt="Supporter logo" />
									<?php } ?>
								</div>
							<?php } ?>
							</div>
						<?php } ?>
						
					</div>

				</div>
			</div>
			
			<?php 
			$projectRelated = new WP_Query(
				array('post_type'=>'post',
					  'posts_per_page'=>-1,
					  'meta_query'=>array(
					  	array('key'=>'related',
					  		  'value'=>get_the_ID(),
					  		  'compare'=>'LIKE',
					  		)
					  	)
					  )
			);
			if ($projectRelated->have_posts()) { 
			?>
			<div class="col col_16 col_first related__column">
				<div class="related__wrapper">
					<div class="col col_2 col_first">&nbsp;</div>
					<div class="col col_12 col_first">						
					<h2 class="h2--related--header">Gerelateerd aan dit project</h2>
					<?php 
					$relatedColumns = 0;
					while ($projectRelated->have_posts()) {
						$continue = false;
						$relatedColumns++;
						$projectRelated->the_post();
						$post_terms = wp_get_post_terms(get_the_ID(),'subject');
						$post_icons = "";
						foreach ($post_terms as $post_term) {
							$post_icon = get_field('subject-icon','subject_'.$post_term->term_id);
							if ($post_icon) {
								$post_icon = file_get_contents($post_icon['url']);
								$post_icons .= $post_icon;
							}
						}
						$post_types = wp_get_post_terms($post->ID,'category');
						$post_types_text = "";
						foreach ($post_types as $post_type) {
							if ($post_type->term_id != 2) {
								$post_types_text = $post_type->name;
								break;
							}
						}
						
						
						
						$relatedItems = get_field('related'); 
						foreach ($relatedItems as $relatedItem) {
							if ($relatedItem->ID == $current_id) {
								$continue = true;
							}
						}
						if ($continue == true) {
						?>
						<div class="col col_third related__item">
							<a href="<?php echo get_permalink($post->ID); ?>">
							<?php if (has_post_thumbnail($post->ID)) { ?>
								<?php echo get_the_post_thumbnail($post->ID,'single-sidebar-thumbnail'); ?>
							<?php } ?>
							<h2 class="h2--related"><?php echo get_the_title($relatedArticle->ID); ?></h2>
							<div class="content__filter__item__metatop">
								<span class="content__filter__item__metatop--category"><?php echo $post_types_text; ?> &nbsp;&bull; </span><span class="content__filter__item__metatop--date"><?php echo get_the_date(); ?> &nbsp;&nbsp; </span><div class="content__filter__item__icons"><?php echo $post_icons; ?></div>
							</div>
							</a>
						</div>
						<?php 
						if ($relatedColumns == 3) { ?><div class="clearfix"></div> <?php $relatedColumns=0; }
					
						}
					} // end while
					?>
					</div>
					<div class="col col_2 col_first">&nbsp;</div>
				</div>
			</div>
			<?php } ?>
			
		</div>
		<!-- Old location -->
	</div>
	
<?php get_footer(); ?>