<?php
	/* 
	Set globals (no need to change by default) 
	*/
	$dir = wp_upload_dir();
	define("UPLOADSFOLDER", $dir['url']);
	define("THEMEFOLDER", 	get_bloginfo('stylesheet_directory'));
	define("IMAGEFOLDER", 	get_bloginfo('stylesheet_directory').'/lib/img');
	define("FONTSFOLDER", 	get_bloginfo('stylesheet_directory').'/lib/fonts');
	define("PHPFOLDER",   	get_theme_root().'/'.get_template().'/api/php');
	define("SVGFOLDER",   	get_theme_root().'/'.get_template().'/lib/img');
	define("RESPONSIVEURL", get_bloginfo('stylesheet_directory').'/api/php/image.ajax.php');
	define("PHPURL",      	get_bloginfo('stylesheet_directory').'/api/php');
	define("JSFOLDER",    	get_bloginfo('stylesheet_directory').'/api/js');
	define("HOMEURL", 	  	site_url());
	define("PAGERROR",	  	5517);
	define("PAGEMAPS",    	7);
	define("PAGEHOME",    	4);
	define("TEXTDOMAIN",  	'kfeg');

	/*
	Enable better image quality
	*/
	add_theme_support( 'post-thumbnails' );
	function bstcm_advanced_compression() {
	    add_theme_support( 'advanced-image-compression' );
	} add_action( 'after_setup_theme', 'bstcm_advanced_compression' );
	
	/* 
	Load external files
	*/
	function bstcm_load_files() {

		if (!is_admin() || (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
			
			wp_register_style( 'font', '//fast.fonts.net/cssapi/ecde81dc-d848-4f66-bef4-b2fface71f14.css' );
			wp_register_style( 'fontawesome-font', THEMEFOLDER.'/lib/css/fontawesome.css' );
			wp_register_style( 'slick-style', '//cdn.jsdelivr.net/jquery.slick/1.5.0/slick.css' );
			wp_register_style( 'default-style', THEMEFOLDER.'/style.css' );
			wp_register_style( 'ui-style', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css');
			wp_register_style( 'pres-style', '/pres/css.css');
			
			wp_enqueue_script('salvattore', JSFOLDER.'/salvattore.min.js', array('jquery'), false, true);
			wp_enqueue_script('jquery-ui', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('jquery'), false, true);
			wp_enqueue_script('touchswipe', '//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.12/jquery.touchSwipe.min.js', array('jquery'), false, true);
	        wp_enqueue_script('bstcm-slickslider', '//cdn.jsdelivr.net/jquery.slick/1.5.0/slick.min.js', array('jquery'), false, true);
	        wp_enqueue_script('bstcm-pointer-events', JSFOLDER.'/pointer-events-polyfill.js', array('jquery'), false, true);
	        wp_enqueue_script('bstcm-waypoints', JSFOLDER.'/jquery.waypoints.min.js', array('jquery'), false, true);
	        wp_enqueue_script('bstcm-waypoints-inview', JSFOLDER.'/inview.min.js', array('jquery'), false, true);	        
	        wp_enqueue_script('functions', JSFOLDER.'/functions.js', array('jquery'), false, true);
	        wp_enqueue_script('pres', '/pres/js.min.js', array('jquery'), false, true);
	        
	        wp_localize_script('functions', 'the_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	        
	        
	        if (!is_page(44)) {
		        wp_enqueue_script('maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyA1klDXOqlfQXJWpI2tLteTGTSf3VZJUYM', array('jquery'), false, true);
		        wp_enqueue_script('markers', JSFOLDER.'/markerwithlabel.min.js', array('jquery'), false, true);
		        wp_enqueue_script('clusters', JSFOLDER.'/markerclusters.min.js', array('jquery'), false, true);
		    }
	
			wp_enqueue_style( 'pres-style' );
	        wp_enqueue_style( 'default-style' );
	        wp_enqueue_style( 'font' );
	        wp_enqueue_style( 'fontawesome-font' );
	        wp_enqueue_style( 'ui-style' );
	        wp_enqueue_style( 'slick-style' );
		
		} else {
			
		    add_editor_style( '//fast.fonts.net/cssapi/ecde81dc-d848-4f66-bef4-b2fface71f14.css');
		    add_editor_style( 'editor-style.css' );
			wp_enqueue_script('bstcm-backend', JSFOLDER.'/backend.js', array('jquery'), false, true);			
			
		}

	}

	//add_action( 'admin_enqueue_scripts', 'bstcm_load_files' );
	add_action( 'wp_enqueue_scripts', 'bstcm_load_files' );


	/*
	Include custom post types
	*/
	require_once(PHPFOLDER."/cpt.inc.php");
	
	
	/* 
	Configure WP menu holders 
	*/
	register_nav_menu( 'primary', 	__( 'Hoofdmenu', TEXTDOMAIN ) );
	register_nav_menu( 'secundary', __( 'Snelmenu', TEXTDOMAIN ) );
	register_nav_menu( 'footer',	__( 'Footermenu', TEXTDOMAIN ) );
	register_nav_menu( 'mobile', 	__( 'Mobielmenu', TEXTDOMAIN ) );


	/*
	Enable Ajax calls (register ajaxurl variable) 
	*/
	function bstcm_set_global_ajaxurl() {
		echo "<script type='text/javascript'>var ajaxurl = '".admin_url('admin-ajax.php')."';</script>";
	} add_action('wp_head','bstcm_set_global_ajaxurl');
	
	
	/*
	Set-up custom image sizes
	*/
	add_image_size( "header-backdrop-visual", 1600, 1200, true);
	add_image_size( "project-loop-thumbnail", 668, 810, true);
	add_image_size( "nieuws-loop-thumbnail", 668, 465, true);
	add_image_size( "single-sidebar-thumbnail", 633, 423, true);
	add_image_size( "content-slideshow-slide", 1336, 846, true);
	add_image_size( "content-sidebar-logo", 257, 168, false);
	add_image_size( "content-lightbox-gallery", 1280, 960, false);
	add_image_size( "content-article-top", 1280, 768, true);
		
	
	/* 
	Enable ACF option fields 
	*/
	if( function_exists('acf_add_options_page') ) { 
	    acf_add_options_page(array(
			'page_title' 	=> 'Website instellingen',
			'menu_title'	=> 'Site opties',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'manage_options',
			'icon_url'		=> 'dashicons-admin-tools',
			'redirect'		=> true
		));
	    
	    acf_add_options_sub_page(array(
			'page_title' 	=> 'Algemene instellingen',
			'menu_title'	=> 'Algemeen',
			'parent_slug'	=> 'theme-general-settings',
			'capability'	=> 'manage_options',
		));
	    				
		acf_set_options_page_capability( 'manage_options' );
	    
	} else {
		echo "<pre>acf_add_options_page not found</pre>";
	}


	/*
	Modify wp-admin columns
	*/	
	Jigsaw::add_column("page", "Opmaak", function($post_id) {
		$currentTemplate = get_page_template_slug($post_id);
		$templates = wp_get_theme()->get_page_templates();
		$templateName = $templates[$currentTemplate];
		echo $templateName;
	}, 3);


	/*
	Function to load filter items through ajax
	*/
	function load_filter_items() {
		get_template_part("modules/get","filter-items");
		die();
	}
	add_action( 'wp_ajax_load_filter_items_hook', 'load_filter_items' );
	add_action( 'wp_ajax_nopriv_load_filter_items_hook', 'load_filter_items' );
	

	/*
	Gravity Forms button modifications
	*/
	add_filter( 'gform_submit_button', 'modify_gf_button', 10, 2 );
	function modify_gf_button($button,$form) {
		return "<button class='' id='gform_submit_button_{$form['id']}'>aanmelden ".bstcm_load_svg('kfeg_arrow_right.svg',false,false)."</button>";
	}

	add_filter( 'gform_submit_button_1', 'modify_nieuwsbrief_button', 10, 2 );
	function modify_nieuwsbrief_button($button,$form) {
		return "<button class='' id='gform_submit_button_{$form['id']}'>".bstcm_load_svg('kfeg_arrow_right.svg',false,false)."</button>";
	}



	/*
	Convert hex colors to rgb colors
	*/
	function hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);
	
	   if(strlen($hex) == 3) {
	      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
	      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
	      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
	      $r = hexdec(substr($hex,0,2));
	      $g = hexdec(substr($hex,2,2));
	      $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   return $rgb; // returns an array with the rgb values
	}
	

	/* 
	Add formats to MCE 
	*/
	add_filter( 'mce_buttons', 'wpex_style_select' );
	if ( ! function_exists( 'wpex_style_select' ) ) {
		function wpex_style_select( $buttons ) {
			array_push( $buttons, 'styleselect' );
			return $buttons;
		}
	}
	
	
	/*
	Add styles to MCE
	*/
	//add_filter( 'tiny_mce_before_init', 'wpex_styles_dropdown' );
	if ( ! function_exists( 'wpex_styles_dropdown' ) ) {
		function wpex_styles_dropdown( $settings ) {
			// Create array of new styles
			$new_styles = array(
				array(
					'title'	=> __( 'Extra styling', 'wpex' ),
					'items'	=> array(
						array(
							'title'		=> 'Opgemaakte link',
							'classes'	=> 'content__wrapper__attachment-item',
							'selector'  => 'a',
							'wrapper'	=> true,
						),
					),
				),
			);
	
			// Merge old & new styles
			$settings['style_formats_merge'] = true;
	
			// Add new styles
			$settings['style_formats'] = json_encode( $new_styles );
	
			// Return New Settings
			return $settings;
		}
	}
		
	/*
	Set or switch region session for current user
	*/
	function bwh_set_region_session($region) {
		/*
  		$wp_session = WP_Session::get_instance();
		if ($region == "-") {
			$wp_session['region'] = false;
		} elseif ($region) { 
			$wp_session['region'] = $region;
		} elseif (!$wp_session) {
			$wp_session['region'] = false;
		}
		*/
	} 
	
	function bwh_get_region_session() {
		/*
  		$wp_session = WP_Session::get_instance();
		if (!$wp_session['region']) { $wp_session['region'] = "nederland"; }
		return $wp_session['region'];
		*/
	}
	
	/*
	Filter posts based on region
	*/
	function bwh_filter_posts_by_region($query) {
		if ( !is_admin() && is_search() && bwh_get_region_session()) {
			$taxquery = array(
		        array(
		            'taxonomy' => 'regio',
		            'field' => 'slug',
		            'terms' => array(bwh_get_region_session()),
		            'operator'=> 'IN'
		        )
		    );
			$query->set('tax_query', $taxquery);
			
		}
	} //add_action('pre_get_posts','bwh_filter_posts_by_region');



	//if ( !is_admin() ) {
		include('api/tweet-php/TweetPHP.php'); 
		add_action('wp_ajax_twitter_feed', 'twitter_feed');
		add_action('wp_ajax_nopriv_twitter_feed', 'twitter_feed');
	//}
	
	/*
	Twitterfeed
	*/ 
	function twitter_feed() {
	
		// External includes
		//include('dist/assets/tweet-php/TweetPHP.php');
		
		$upload_dir = wp_upload_dir();
		
		$template  = '<div class="content__filter__item__meta--small"><a href="https://twitter.com/keninggreide" target="_blank">TWEETS VAN @KENINGGREIDE</a></div><ul class="tweetslider" id="twitter">{tweets}</ul>';
		$template .= '<div class="social__share social__share--tweetslider">'.
					 '<a class="social__share__icon" target="_blank" href="https://www.facebook.com/KeningfaneGreide/" title="Facebook">'.
					 bstcm_load_image('kfeg_picto-facebook-black.svg',false,false,false).
					 '</a>'.
					 '<a class="social__share__icon social__share__icon--twitter" target="_blank" href="https://twitter.com/keninggreide" title="Twitter">'.
					 bstcm_load_image('kfeg_picto-twitter-black.svg',false,false,false).
					 '</a>'.
					 '</div>';
		
		$tweetPHP = new TweetPHP(
		          array(
		            'consumer_key'  		=>  'dGp8RJxbpl10zJnyPUAkUtlJh',
		            'consumer_secret'  		=>  '10gz9Fq15Rc0fZfeibxGdBWyjWIwqCsKp7KVVOD6lXIhEyRZiN',
		            'access_token'  		=>  '219307081-IFOUMG0ZvpXQSC86fKYJBtMTAa5G1FqkdSjXIHoK',
		            'access_token_secret'  	=>  'KXXkQf4D9A2H2QX2gBgvokVWwSIe0HHRZgz3FDCCBJXDn',
		            'twitter_screen_name'  	=>  'KeningGreide',
		            'enable_cache'          => false,
		            'cache_dir' 			=> $upload_dir['basedir'].'/tweets/',
		            'tweets_to_retrieve'    => 10,
		            'tweets_to_display' 	=> 5,
		            'ignore_replies'        => true, // Ignore @replies
		            'ignore_retweets'       => false, // Ignore retweets
		            'twitter_template'      => $template,
		            'tweet_template'        => '<li class="content__filter__item--quote"><blockquote class="content__filter__item__quote">{tweet}</blockquote><a target="_blank" href="{link}">Bekijk op Twitter</a></li>'
		          )
		        );
		  
		echo $tweetPHP->get_tweet_list();
		
		exit;
	
	}

?>