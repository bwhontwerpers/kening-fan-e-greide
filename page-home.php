<?php error_log("begin"); ?>
<?php 
/* Template name: Start pagina */ 
get_header();
if (empty($_GET['filter'])) {
	$customfilter = "uitgelicht";
} else {
	$customfilter = $_GET['filter'];
}

global $totalposts,$markers,$customfilter;
if (bwh_get_region_session()) {
	$taxQuery = array(
			        array(
			            'taxonomy' => 'regio',
			            'field' => 'slug',
			            'terms' => array(bwh_get_region_session()),
			            'operator' => 'IN',
			        )
			    );
} else {
	$taxQuery = array();
}

$itemsQuery = new WP_Query(
	array('post_type'=>'post',
		  'posts_per_page'=>-1,
		  'no_found_rows' => true,
		  'update_post_term_cache' => false,
		  'update_post_meta_cache' => false,
		  'tax_query'=>$taxQuery,
		  'category_name'=>$customfilter,
		  'orderby'=>'date',
		  'order'=>'DESC',
	)
);

$totalposts = $itemsQuery->post_count;
while ($itemsQuery->have_posts()) { 
	$itemsQuery->the_post();
	$markers[] = $post;
} wp_reset_postdata();
?>

<?php error_log("eind"); ?>

<?php while (have_posts()) { the_post(); ?>
	
	<div class="row content dynamic_width">
				
		<div class="section">
			<div class="col col_16 col_first ">
				<div class="content__wrapper">
					<div class="site_overlay"></div>			
					<?php 
					foreach ($markers as $marker) {
						$terms = wp_get_post_terms($marker->ID,'subject');
						foreach ($terms as $term) {
							$post_icon = get_field('subject-icon','subject_'.$term->term_id);
							if ($post_icon) {
								$post_icon_url = $post_icon['url'];
								break;
							}
						}
						
						if (get_field('post-locations',$marker->ID)) {
							$locations = get_field('post-locations',$marker->ID);
							foreach ($locations as $location) { 
								echo "<datalist id='{$marker->post_name}' data-icon='".$post_icon_url."' data-title='".get_the_title($marker->ID)."' data-link='".get_permalink($marker->ID)."' data-lat='{$location['post-location']['lat']}' data-lng='{$location['post-location']['lng']}'></datalist> \n\r";
							}
						}
					}
					?>
					<div class="content__mapper">&nbsp;</div>
					
					<div class="content__switch__view" onclick="toggle_jumbo_map();">
						<svg version="1.1" id="mapicon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="-36 37 25.5 27" style="enable-background:new -36 37 25.5 27;" xml:space="preserve" width="25.5" height="27">
						<style type="text/css">
							.st0{fill:#FFFFFF;}
						</style>
						<path class="st0" d="M-36,42.4c0-0.9,0-1.3,0-1.7c0-0.6,0.3-1.1,0.9-1.4c1-0.5,2-1.1,3.1-1.5c1.5-0.6,3.1-0.7,4.6,0.2
							c0.8,0.5,1.7,0.9,2.5,1.4c0.7,0.4,1.5,0.5,2.2,0.2c1.3-0.5,2.7-1,3.7-2c0.5-0.5,1.1-0.7,1.8-0.6c1.2,0.1,2.5,0.2,3.7,0.5
							c0.8,0.2,1.5,0.7,2.3,1.2c0.5,0.3,0.7,0.9,0.7,1.5c0,0.6,0,1.1-0.1,1.7c-0.5,3-0.3,6.1-0.1,9.1c0.1,1.3,0,2.5-0.2,3.7
							c-0.1,0.7-0.4,1-1.1,1c-0.4,0-0.8,0-1.1,0c-0.6,0-1.1,0.4-1.4,0.9c-1.1,2.2-2.2,4.4-3.2,6.7c-0.4,0.8-1.2,1-1.8,0.3
							c-0.2-0.2-0.4-0.4-0.5-0.7c-0.5-0.6-1-1.2-1.4-1.8c-1.1-1.9-2.7-3.2-4.6-4.1c-0.4-0.2-0.8-0.5-1.3-0.7c-0.8-0.4-1.6-0.5-2.4,0
							c-1.2,0.8-2.5,1.3-4,1.5c-0.9,0.1-1.5-0.3-1.5-1.2c-0.1-1.6-0.1-3.2-0.2-4.9c-0.1-2.7,0-5.4-0.5-8.2C-36,43-36,42.4-36,42.4z
							 M-11.8,47.2h-0.1c0-0.7,0-1.3,0-2c0-0.8,0.1-1.5,0.1-2.3c0-1,0-1.9-0.2-2.8c-0.1-0.4-0.4-0.9-0.7-1.1c-1-0.7-4.6-0.8-5.6-0.3
							c-0.1,0-0.2,0.1-0.2,0.2c-1,1.2-2.5,1.3-3.7,2c-0.9,0.5-1.8,0.3-2.7-0.2c-1-0.7-1.9-1.2-2.8-1.7c-1.2-0.7-2.6-0.7-3.8-0.2
							c-1,0.4-1.8,1-2.7,1.5c-0.3,0.3-0.6,0.7-0.6,1c-0.1,0.8,0,1.5,0.1,2.3c0.1,1,0.3,2.1,0.4,3.1c0.1,1.6,0,3.2,0.1,4.9
							c0,1.2,0.1,2.5,0.3,3.7c0.1,1.1,0.8,1.4,1.9,1c0.7-0.3,1.4-0.6,2-1.1c0.8-0.6,1.5-0.6,2.4-0.1c1,0.5,2.1,0.9,3.2,1.3
							c0.2,0.1,0.6-0.2,0.7-0.3c0.1-0.3,0.1-0.7,0.1-1.1c0.1-1,0-2.1,0.2-3.1c0.5-2.5,2.4-3.8,5-3.9c1.5-0.1,3.9,1.1,4.5,3.7
							c0.2,0.6,0.2,1.3,0.4,2c0.1,0.5,0.2,0.9,0.8,1c0.6,0.1,1-0.3,1.1-1.1c0-0.5,0.1-0.9,0.1-1.4C-11.8,50.4-11.8,48.8-11.8,47.2z
							 M-15.1,53.6c-0.1-0.7-0.2-1.5-0.4-2.2c-0.3-1-0.9-1.6-1.9-2c-1.9-0.8-4.3,0.3-4.8,2.4c-0.6,2.3-0.8,4.7,0.9,6.8
							c0.2,0.3,0.3,0.7,0.5,1c0.4,0.7,0.8,1.3,1.2,1.9c0.4,0.6,1,0.6,1.4,0c0.8-1.3,1.5-2.6,2.2-4C-15.5,56.2-15.1,55-15.1,53.6z"/>
						<path class="st0" d="M-20.8,53c0-0.2,0.1-0.6,0.2-1c0.5-1.4,2-1.7,3-0.5c0.5,0.6,0.9,1.3,0.6,2.1c-0.5,1.2-1.7,1.7-2.9,1.1
							C-20.6,54.4-20.8,53.8-20.8,53z M-18.1,53.3c0.1-0.6-0.2-1.1-0.7-1.3c-0.2-0.1-0.5,0-0.6,0.1c-0.5,0.5-0.5,1.2-0.2,1.7
							c0.2,0.3,0.6,0.4,0.8,0.2C-18.4,53.7-18.2,53.4-18.1,53.3z"/>
						<path class="st0" d="M-28.7,51c0,0.3,0,0.6,0,0.9c0,0.4-0.4,0.9-0.6,0.7c-0.2-0.1-0.5-0.3-0.6-0.5c-0.1-0.2-0.1-0.5-0.2-0.8
							c0-2.9,0-5.7-0.1-8.6c0-0.2,0-0.4,0.1-0.5c0.1-0.2,0.3-0.3,0.4-0.3c0.1,0,0.3,0.2,0.4,0.3c0.1,1.2,0.2,2.5,0.2,3.7
							C-29,47.6-28.9,49.3-28.7,51L-28.7,51z"/>
						<path class="st0" d="M-18.3,44.6c0-1.6,0-2.8,0.6-3.8c0.1-0.1,0.3-0.2,0.4-0.2s0.4,0.3,0.3,0.3c-0.5,1.5-0.3,3-0.2,4.5
							c0,0.4-0.1,0.7-0.2,1.1c0,0.1-0.3,0.2-0.4,0.2s-0.2-0.1-0.2-0.1C-18.1,45.8-18.2,44.9-18.3,44.6z"/>
						<path class="st0" d="M-23.9,46.3c0,0.8,0,1.4,0,2c0,0.1-0.2,0.3-0.4,0.3c-0.1,0-0.4-0.1-0.4-0.2c-0.3-1.5-0.3-3.1,0.4-4.6
							c0,0,0.2,0,0.3,0s0.2,0.1,0.2,0.1c0.1,0.2,0.1,0.3,0.1,0.5C-23.7,45.1-23.8,45.8-23.9,46.3z"/>
						</svg>
						<svg version="1.1" id="mapclose" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="-40 41.6 18.2 18.4" style="enable-background:new -40 41.6 18.2 18.4;" xml:space="preserve" width="18.2" height="18.4">
						<style type="text/css">
							.st0{fill:#FFFFFF;}
						</style>
						<path class="st0" d="M-38.8,60c-0.2-0.1-0.4-0.1-0.4-0.1c-0.7-0.5-1-1.8-0.5-2.5c1.7-2.2,3.4-4.4,5.5-6.2c0.4-0.3,0.7-0.6,1-1
							c0.1-0.2,0.2-0.6,0-0.8c-0.2-0.3-0.5-0.7-0.9-1c-1.1-1-2.3-2-3.4-3.1c-0.6-0.6-1.3-1.3-1.9-2.1c-0.4-0.5-0.3-1.1,0.2-1.4
							c0.4-0.3,0.9-0.2,1.3,0.2c0.9,1,1.9,2.1,2.9,3.1c0.9,0.9,2,1.8,3,2.6c0.6,0.5,1.1,0.5,1.8,0c0.5-0.4,1-0.8,1.5-1.3
							c1.5-1.3,3.1-2.6,4.6-3.8c0.2-0.2,0.5-0.4,0.7-0.4s0.6,0,0.7,0.2c0.2,0.2,0.3,0.6,0.2,0.8c-0.2,0.4-0.5,0.8-0.8,1.2
							c-0.9,0.8-1.9,1.5-2.8,2.4c-0.9,0.8-1.9,1.7-2.8,2.7c-0.5,0.5-0.4,1,0.2,1.5c2.3,1.9,4.2,4.1,6.1,6.4c0.3,0.4,0.6,1,0.8,1.5
							c0.2,0.3-0.2,0.8-0.6,0.8c-0.9-0.1-1.6-0.4-2.2-1.2c-1-1.4-2.1-2.7-3.2-4c-0.6-0.7-1.3-1.3-2-2c-0.5-0.5-1-0.5-1.5,0
							c-1.2,1.2-2.4,2.4-3.6,3.7c-1,1.1-2,2.3-3.1,3.3C-38.2,59.8-38.6,59.9-38.8,60z"/>
						</svg>
					</div>
					
					<ul class="content__filter__types">
						<?php 
						$menuTypes = get_terms('category', array('hide_empty'=>true,'hierarchical'=>false,'parent'=>0) ); 
						foreach ($menuTypes as $menuType) { 
							if ($customfilter==$menuType->slug) { $class="active"; } else { $class = ""; }
							if ($menuType->slug != "publicaties") {
							?>
							<li class="content__filter__type <?php echo $class; ?>"><a href="/?filter=<?php echo $menuType->slug; ?>" title="<?php echo $menuType->name; ?>"><?php echo $menuType->name; ?></a></li>
							<?php 
							}
						}
						?>
					</ul>
					
					<div class="content__filter__items" data-columns data-custom-filter="<?php echo $customfilter; ?>">
							<div id="twitter" class="loadingtweets"> </div>
						<?php get_template_part("modules/get","filter-items"); ?>
					</div>
					
					<div class="col col_16 col_first content--styled">
						<?php if ($totalposts>14) { ?>
							<?php if ( !$_GET['filter'] || 'uitgelicht' == $_GET['filter'] ) { ?>
								<a href="/?filter=nieuws" class="content__filter__loadmore">Meer nieuws</a>
							<?php } else { ?>
								<a href="javascript:load_filter_items();" id="load_more_items" class="content__filter__loadmore" data-total="<?php echo $totalposts; ?>" data-offset="14" data-limit="14" data-category="<?php echo $customfilter; ?>">Meer laden</a>
							<?php } ?>
						<?php } ?>
					</div>
					
				</div>
				
			</div>

		</div>
		
	</div>

<?php error_log("einde"); ?>

	<?php if ($_GET['filter']) { ?>
	<script>
		jQuery(document).on("ready", function() { jQuery("body,html").animate({'scrollTop':jQuery(window).height()-550},500); }); 
	</script>
	<?php } ?>
	
<?php } ?>

<?php get_footer(); ?>
<?php error_log("einde na footer"); ?>