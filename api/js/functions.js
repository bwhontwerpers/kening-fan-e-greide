/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CUSTOM FRONT-END JS FUNCTIONALITY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
/* 
FRAMEWORK: VARIABLES 
*/
var ajaxurl;
/* 
FRAMEWORK: TOOLS
*/
/* Get internet explorer version */

function get_ie_version() {
	'use strict';
	var rv, ua, re;
	rv = -1;
	if (navigator.appName === 'Microsoft Internet Explorer') {
		ua = navigator.userAgent;
		re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
		if (re.exec(ua) !== null) {
			rv = parseFloat(RegExp.$1);
		}
	} else if (navigator.appName === 'Netscape') {
		ua = navigator.userAgent;
		re = new RegExp("Trident/.*rv:([0-9]{1,}[.0-9]{0,})");
		if (re.exec(ua) !== null) {
			rv = parseFloat(RegExp.$1);
		}
	}
	return rv;
} /* Basename extractor */

function baseName(str) {
	'use strict';
	var base = str.substring(str.lastIndexOf('/') + 1);
	if (base.lastIndexOf(".") !== -1) {
		base = base.substring(0, base.lastIndexOf("."));
	}
	return base;
} /* Pointer events enabler for Internet Explorer */

function enable_pointer_events() {
	if (jQuery("html").hasClass("ie")) {
		PointerEventsPolyfill.initialize({});
	}
} /* Natural sort for arrays */

function sortNumber(a, b) {
	return a - b;
}
/*
FRAMEWORK: FUNCTIONS
*/
/* Enable fullclick on dom elements */

function init_fullclick_enabler() {
	'use strict';
	jQuery(".fullclick").click(function() {
		document.location = jQuery(this).find("a").attr("href");
	});
} /* Sync dom elements with 'auto-sync-height' class */

function init_auto_height() {
	"use strict";
	if (jQuery(".smartphone:visible").length === 0) {
		var syncParent = false;
		var minHeight = 0;
		jQuery(".auto-sync-children").each(function(pnr, parent) {
			var syncHeights = [];
			jQuery(this).find(".auto-sync-height").each(function(cnr, child) {
				syncHeights.push(jQuery(child).innerHeight());
			});
			syncHeights.sort(sortNumber);
			syncHeights.reverse();
			jQuery(parent).find(".auto-sync-height").each(function(cnr, child) {
				jQuery(child).css({
					'height': syncHeights[0]
				});
			});
		});
	}
} /* Enable slick slideshows */

function start_slick_sliders() {
	'use strict' /* Inline content sliders */
	jQuery(".content__slideshow").slick({
		"autoplay": true,
		"slidesToShow": 1,
		"slidesToScroll": 1,
		"dots": false,
		"appendDots": jQuery(".content__slideshow__pager"),
		"fade": false,
		"infinite": true,
		"adaptiveHeight": true,
		"arrows": true,
		"swipeToSlide": true,
		"prevArrow": '<button type="button" class="slick-prev"></button>',
		"nextArrow": '<button type="button" class="slick-next"></button>',
		"customPaging": function(slider, i) {
			return '<button type="button" data-role="none">&nbsp;</button>';
		},
		responsive: [{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true,
				adaptiveHeight: true,
				arrows: false
			}
		}, ]
	});
} /* Set IE tag and version into body element */

function set_ie_version() {
	if (get_ie_version() > 0) {
		var ieVersion = "ie" + get_ie_version();
		jQuery("html").addClass("ie " + ieVersion);
	}
} /* Enable mini Google maps for single post templates */

function init_google_mini_map() {
	var map = false;
	var mapsWrapper = jQuery(".sidebar__wrapper__mapobject");
	if (!map && jQuery(mapsWrapper).length > 0) {
		var styles = [{
			"featureType": "administrative",
			"elementType": "labels.text.fill",
			"stylers": [{
				"color": "#444444"
			}]
		}, {
			"featureType": "administrative.country",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "on"
			}]
		}, {
			"featureType": "administrative.locality",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "simplified"
			}, {
				"lightness": "35"
			}]
		}, {
			"featureType": "administrative.neighborhood",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "administrative.land_parcel",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [{
				"color": "#f2f2f2"
			}]
		}, {
			"featureType": "poi",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "road",
			"elementType": "all",
			"stylers": [{
				"saturation": -100
			}, {
				"lightness": 45
			}]
		}, {
			"featureType": "road.highway",
			"elementType": "all",
			"stylers": [{
				"visibility": "simplified"
			}]
		}, {
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "water",
			"elementType": "all",
			"stylers": [{
				"color": "#8da997"
			}, {
				"visibility": "on"
			}]
		}];
		var bound = new google.maps.LatLngBounds();
		var boundExtended = false;
		var mapsLat = jQuery(".sidebar__wrapper__mapobject").attr("data-lat");
		var mapsLng = jQuery(".sidebar__wrapper__mapobject").attr("data-lng");
		var mapsTitle = jQuery(".sidebar__wrapper__mapobject").attr("data-title");
		var mapsIcon = jQuery(".sidebar__wrapper__mapobject").attr("data-icon");
		var mapsObject = document.getElementsByClassName("sidebar__wrapper__mapobject");
		var mapsLatLng = new google.maps.LatLng(mapsLat, mapsLng);
		var GRAFISCH = "Styled";
		if (jQuery(".smartphone:visible").length) {
			var mapsDraggable = false;
			var mapsScrollwheel = false;
		} else {
			var mapsDraggable = true;
			var mapsScrollwheel = true;
		}
		map = new google.maps.Map(mapsObject[0], {
			center: mapsLatLng,
			zoom: 11,
			panControl: false,
			draggable: mapsDraggable,
			zoomControl: false,
			scrollwheel: mapsScrollwheel,
			mapTypeControl: false,
			mapTypeId: 'Styled',
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false,
			disableDoubleClickZoom: true,
		}); /* Define icon for new marker */
		var icon = {
			url: '/wp-content/themes/keningfanegreide/lib/img/kfeg_maps_marker.png',
		}; /* Create new marker */
		setTimeout(function() {
			var marker = new MarkerWithLabel({
				position: mapsLatLng,
				draggable: false,
				raiseOnDrag: false,
				map: map,
				icon: icon,
				title: mapsTitle,
				labelAnchor: new google.maps.Point(30, 45),
				labelContent: "<img src='" + mapsIcon + "' />",
				labelClass: "content__mapper__marker",
				labelInBackground: true,
			});
		}, 1000);
		var styledMapType = new google.maps.StyledMapType(styles, {
			name: "Styled"
		});
		map.mapTypes.set(GRAFISCH, styledMapType);
	}
} /* Scroll to content based on target */

function scroll_to_content(target) {
	if (jQuery("[data-page-slug=" + target + "]").length) {
		var scroll_target = jQuery("[data-page-slug=" + target + "]").offset().top;
		if (jQuery(".smartphone:visible").length) {
			scroll_target -= 74;
		}
		jQuery("body,html").animate({
			'scrollTop': scroll_target
		}, 750);
	}
} /* Overlay buttons interactivity */

function init_overlay_hooks() {
	jQuery("a[rel=trigger-overlay]").on("click", function(element) {
		var target = jQuery(this).attr('href');
		jQuery("[data-overlay-source='" + target + "']").toggleClass("visible").css({
			'padding-top': jQuery(document).scrollTop()
		});

		element.preventDefault();
	});
	jQuery(".comp_page_overlay a[href=#close]").on("click", function(element) {
		jQuery(".comp_page_overlay.visible").removeClass("visible");
		element.preventDefault();
	});
} /* Store maps markers into array */
var map_markers = [];

function storeMarker(markerId, markerLng, markerLat, markerHtml, markerStyle) {
	map_markers.push([markerLng, markerLat, markerHtml, markerStyle]);
} /* LIJST/KAART SELECTION EFFECT */

function doSort(sortField, sortOrder) {
	jQuery("input[name=sort]").val(sortField);
	jQuery("input[name=order]").val(sortOrder);
	jQuery("form[name=sortform]").submit();
} /* STICKY HEADER */

function init_waypoints_header() {
	
	if (jQuery("body").hasClass("home") && !jQuery(".tabletlandscape:visible").length) {
		
		var offset = 600;
		var waypoints = jQuery('.content').waypoint(

		function(direction) {
			if (direction == 'down') {
				
				if (!jQuery("body").hasClass("state_nav_open")) {
					jQuery(".header__visual__logo").fadeOut(300, function() {
						jQuery('body').addClass("minimal");
					});
				}
			} else {
				if (!jQuery("body").hasClass("state_nav_open")) {
					jQuery('body').removeClass("minimal");
					setTimeout(function() {
						jQuery(".header__visual__logo").fadeIn(300);
					}, 300);
				}
			}
		}, {
			offset: offset
		})
		var secondOffset = jQuery(".header__visual__navitems").offset().top;
		var waypoints = jQuery('.content').waypoint(

		function(direction) {
			if (direction == 'down') {
				jQuery('body').addClass("minimal--default");
			} else {
				jQuery('body').removeClass("minimal--default");
			}
		}, {
			offset: secondOffset
		})
		
	} else if (jQuery("body").hasClass("home") && jQuery(".tabletlandscape:visible").length) {

		var offset = 400;
		var waypoints = jQuery('.content').waypoint(		
			function(direction) {
				if (direction == 'down') {
					jQuery(".header__visual__logo").fadeOut(300);
					jQuery(".header__visual__brand").fadeOut(300);
				} else {
					jQuery(".header__visual__logo").fadeIn(300);
					jQuery(".header__visual__brand").fadeIn(300);
				}
			}, { offset: offset }
		);
		
	} else {
				
		var offset = jQuery(".header__visual__textlogo").offset().top + jQuery(".header__visual__textlogo").innerHeight();
		var waypoints = jQuery('.content').waypoint(

		function(direction) {
			if (direction == 'down') {
				jQuery('body').addClass("minimal--default");
			} else {
				jQuery('body').removeClass("minimal--default");
			}
		}, {
			offset: '130px'
		})
	}
	
	/* Waypoint mobile */
	var waypoints = jQuery('.content').waypoint(

		function(direction) {
			if (direction == 'down') {
				jQuery(".header__visual__logo").addClass("animated");
				jQuery('body').addClass("mobile--snap");
			} else {
				jQuery('body').removeClass("mobile--snap");
				setTimeout(function() { 
					jQuery(".header__visual__logo").removeClass("animated");
				}, 500);
				
			}
		}, {
			offset: '130px'
		}
	)
	
	if (jQuery(".content__wrapper__top__wrapper:visible").length && !jQuery(".smartphone:visible").length ) {
		
		/* Waypoint mobile */
		var waypoints = jQuery('.content__wrapper').waypoint(
	
			function(direction) {
				if (direction == 'down') {
					jQuery('body').addClass('show--breadcrumb');
					console.log("Show scroll to top");
				} else {
					jQuery('body').removeClass('show--breadcrumb');
					console.log("Hide scroll to top");
				}
			}, {
				offset: '-200px'
			}
		);
		
	}
		
}

function init_header_height() {

	if (jQuery("[data-height-offset]").length) {
		jQuery("[data-height-offset]").each(function(nr, el) {
			var viewport = jQuery(window).height();
			var offset = jQuery(el).attr('data-height-offset');
			if (jQuery(".smartphone:visible").length && jQuery(el).hasClass("header__mobilenav")) { offset = -100; }
			if (jQuery(".smartphone:visible").length && jQuery(el).hasClass("header")) { offset = 100; }
			var height = viewport - offset;
			jQuery(el).css({
				'height': height
			});
		});
		if (jQuery("body").hasClass("home")) {
			jQuery("body").css({
				'padding-top': jQuery(".header").height()
			});
		}
	}
	var grid = document.querySelector('.content__filter__items');
	if (grid) {
		salvattore.recreateColumns(grid);
	}
	
	if (!jQuery(".smartphone:visible").length) {
		var dynamicWidth = Math.round(jQuery(window).width() - 180);
		if (1 === (dynamicWidth % 2)) {
			dynamicWidth += 1;
		}
		jQuery(".dynamic_width").css({
			'width': dynamicWidth
		});
	}
	
}

function init_search_module() {
	jQuery("[rel=init_search_module]").on("click", function(event) {
		// Initiate search/submit when search input contains value
		if (jQuery(".content__sidebar__search").val()) {
			jQuery("form[name=search").submit();
			// Show hide search panel when search input is empty	
		} else {
			if (jQuery("body").hasClass("state_nav_open")) {
				// Close
				jQuery(".header__visual__icon").trigger("click");
				jQuery(".content__sidebar__search").blur();
				setTimeout(function() {
					if (!jQuery(".tabletlandscape:visible").length) {
						jQuery(".header__visual__logo").fadeIn(300);
					}
				}, 450);
			} else {
				// Open (homepage)
				if (jQuery("body").hasClass("home")) {
					if (!jQuery(".tabletlandscape:visible").length) {
						jQuery(".header__visual__logo").fadeOut(300, function() {
							jQuery(".header__visual__icon").trigger("click");
							jQuery(".content__sidebar__search").focus();
						});
					} else {
						jQuery(".header__visual__icon").trigger("click");
						jQuery(".content__sidebar__search").focus();						
					}
					// Open (other pages)
				} else {
					jQuery(".header__visual__icon").trigger("click");
					jQuery(".content__sidebar__search").focus();
				}
			}
		}
		event.preventDefault();
	});
	
	jQuery("[rel=init_region_module]").on("click", function(event) {
		if (jQuery("body").hasClass("state_nav_open")) {
			// Close
			jQuery(".header__visual__icon").trigger("click");
			jQuery(".content__sidebar__search").blur();
			setTimeout(function() {
				if (!jQuery(".tabletlandscape:visible").length) {
					jQuery(".header__visual__logo").fadeIn(300);
				}
			}, 450);
		} else {
			// Open (homepage)
			if (jQuery("body").hasClass("home")) {
				if (!jQuery(".tabletlandscape:visible").length) {
					jQuery(".header__visual__logo").fadeOut(300, function() {
						jQuery(".header__visual__icon").trigger("click");
					});
				} else {
					jQuery(".header__visual__icon").trigger("click");
				
				}
				// Open (other pages)
			} else {
				jQuery(".header__visual__icon").trigger("click");
			}
		}

		event.preventDefault();
	});
}
var filter_items_offset = 0;
var filter_items_category = 0;
var filter_items_limit = 0;
var filter_items_total = 0;
var filter_load_label = "";

function load_filter_items() {
	filter_items_total = parseInt(jQuery('#load_more_items').attr('data-total'));
	filter_items_limit = parseInt(jQuery('#load_more_items').attr('data-limit'));
	filter_items_offset = parseInt(jQuery('#load_more_items').attr('data-offset'));
	filter_items_category = jQuery('#load_more_items').attr('data-category');
	
	filter_load_label = jQuery("#load_more_items").html();
	
	jQuery("#load_more_items").html("...").fadeTo(250, 0.5);
	jQuery('#load_more_items').attr('data-offset', (filter_items_limit + filter_items_offset));
	jQuery.post(the_ajax_script.ajaxurl, {
		action: "load_filter_items_hook",
		offset: filter_items_offset,
		category: filter_items_category,
		output: 'json'
	}, function(results) {
		results = JSON.parse(results);
		var grid = document.querySelector('.content__filter__items');
		var items = new Array();
		for (var i = 0; i < results.length; i++) {
			var obj = results[i];
			var item = document.createElement('div');
			jQuery(item).addClass('filter__item__wrapper filter__item__wrapper--'+filter_items_category).html(obj).css({
				'opacity': 0,
				'margin-top': '30px'
			});
			items.push(item);
			jQuery(item).delay(i * 100).animate({
				'opacity': 1,
				'margin-top': '0px'
			}, 250);
		}
		salvattore.appendElements(grid, items);
		if ((filter_items_limit + filter_items_offset) >= filter_items_total) {
			jQuery('#load_more_items').slideUp();
		} else {
			jQuery("#load_more_items").html(filter_load_label).fadeTo(250, 1.0);
		}
	});
}
var globalWindowScroll;
var globalWindowOffset;

function init_switch_menu() {
	jQuery(".header__visual__icon,.header__visual__close,.site_overlay,.header__visual__navitem img").on("click", function(event) {
		if (!jQuery(".tabletlandscape:visible").length && !jQuery(".smartphone:visible").length) {
						
			if (jQuery("body").hasClass("state_nav_open")) {
				jQuery(".site_overlay").fadeOut(250, function() {
					jQuery("body").removeClass("state_nav_open");
					jQuery(window).scrollTop(globalWindowScroll);
				});
			} else {
				
				if (jQuery("body").hasClass("home")) {
					globalWindowOffset = -190;
				} else {
					globalWindowOffset = (150) - jQuery(window).height();
				}
				globalWindowScroll = jQuery(window).scrollTop();
				jQuery(".site").css({
					'top': ((globalWindowOffset) - (globalWindowScroll - jQuery(window).height()))
				});
				jQuery("body").addClass("state_nav_open");
				jQuery(".site_overlay").fadeIn(250);
				jQuery(".site").trigger('click');
				event.preventDefault();
			}
			
		} else if (!jQuery(".smartphone:visible").length) {
						
			if (jQuery("body").hasClass("state_mobilenav_open")) {
				jQuery(".site_overlay").fadeOut(250, function() {
					jQuery("body").removeClass("state_mobilenav_open");
					jQuery(window).scrollTop(globalWindowScroll);
				});
			} else {
			
				if (jQuery("body").hasClass("home")) {
					globalWindowOffset = -190;
				} else {
					globalWindowOffset = (150) - jQuery(window).height();
				}
				
				globalWindowScroll = jQuery(window).scrollTop();
				jQuery(".site").css({
					'top': ((globalWindowOffset) - (globalWindowScroll - jQuery(window).height()))
				});
				
				jQuery("body").addClass("state_mobilenav_open");
				jQuery(".site_overlay").fadeIn(250);
				jQuery(".site").trigger('click');
				event.preventDefault();
				
			}
			
				
		} else {
						
			if (jQuery("body").hasClass("state_mobilenav_open")) {
				jQuery(".site_overlay").fadeOut(250, function() {
					jQuery("body").removeClass("state_mobilenav_open");
				});
			} else {		
				jQuery("body").addClass("state_mobilenav_open");
				jQuery(".site").trigger('click');
				event.preventDefault();
				
			}
			
		}
		
	});
} /* Initialize large jumbo map */
var map = false;

function init_jumbo_map() {
	var mapsWrapper = jQuery(".content__mapper");
	if (!map && jQuery(mapsWrapper).length > 0) {
		map = true;
		var styles = [{
			"featureType": "administrative",
			"elementType": "labels.text.fill",
			"stylers": [{
				"color": "#444444"
			}]
		}, {
			"featureType": "administrative.country",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "on"
			}]
		}, {
			"featureType": "administrative.locality",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "simplified"
			}, {
				"lightness": "35"
			}]
		}, {
			"featureType": "administrative.neighborhood",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "administrative.land_parcel",
			"elementType": "labels.text",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [{
				"color": "#f2f2f2"
			}]
		}, {
			"featureType": "poi",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "road",
			"elementType": "all",
			"stylers": [{
				"saturation": -100
			}, {
				"lightness": 45
			}]
		}, {
			"featureType": "road.highway",
			"elementType": "all",
			"stylers": [{
				"visibility": "simplified"
			}]
		}, {
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "water",
			"elementType": "all",
			"stylers": [{
				"color": "#8da997"
			}, {
				"visibility": "on"
			}]
		}]; /* Define maps parameters */
		var bound = new google.maps.LatLngBounds();
		var boundExtended = false;
		var mapsLat = 52.553519; //jQuery(".content_mapper").attr("data-lat");
		var mapsLng = 5.996094; //jQuery(".content_mapper").attr("data-lng");
		var mapsTitle = ''; //jQuery(".content_mapper").attr("data-title");
		var mapsObject = document.getElementsByClassName("content__mapper");
		var mapsLatLng = new google.maps.LatLng(mapsLat, mapsLng);
		var clusterMarkers = new Array();
		var GRAFISCH = "Styled";
		if (jQuery(".smartphone:visible").length) {
			var mapsDraggable = false;
			var mapsScrollwheel = false;
		} else {
			var mapsDraggable = true;
			var mapsScrollwheel = true;
		} /* Create Google maps object */
		map = new google.maps.Map(mapsObject[0], {
			center: mapsLatLng,
			zoom: 11,
			panControl: false,
			draggable: mapsDraggable,
			zoomControl: false,
			scrollwheel: mapsScrollwheel,
			mapTypeControl: false,
			mapTypeId: 'Styled',
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false,
			disableDoubleClickZoom: false,
		}); /* Create markers based on <datalist> objects */
		jQuery("datalist").each(function(nr, dataset) { /* Define position for new marker */
			markerLatLng = new google.maps.LatLng(jQuery(dataset).attr('data-lat'), jQuery(dataset).attr('data-lng')); /* Define icon for new marker */
			var icon = {
				url: '/wp-content/themes/keningfanegreide/lib/img/kfeg_maps_marker.png',
			}; /* Create new marker */
			var marker = new MarkerWithLabel({
				position: markerLatLng,
				draggable: false,
				raiseOnDrag: false,
				map: map,
				icon: icon,
				title: jQuery(dataset).attr('data-title'),
				link: jQuery(dataset).attr('data-link'),
				labelAnchor: new google.maps.Point(30, 45),
				labelContent: "<img src='" + jQuery(dataset).attr('data-icon') + "' />",
				labelClass: "content__mapper__marker",
				labelInBackground: true,
			}); /* Add click handler for new marker */
			if (jQuery(dataset).attr('data-link').length) {
				marker.addListener('click', function() {
					document.location = jQuery(this).attr('link');
				});
			} /* Extend maps bound with new marker */
			bound.extend(markerLatLng); /* Add marker to cluster array */
			clusterMarkers.push(marker);
		}); /* Extend bounds to zoom out for single markers */
		if (bound.getNorthEast().equals(bound.getSouthWest())) {
			var extendPoint1 = new google.maps.LatLng(bound.getNorthEast().lat() + 0.01, bound.getNorthEast().lng() + 0.05);
			var extendPoint2 = new google.maps.LatLng(bound.getNorthEast().lat() - 0.01, bound.getNorthEast().lng() - 0.05);
			bound.extend(extendPoint1);
			bound.extend(extendPoint2);
		}
		var styledMapType = new google.maps.StyledMapType(styles, {
			name: "Styled"
		});
		map.mapTypes.set(GRAFISCH, styledMapType);
		if (jQuery("datalist").length) {
			setTimeout(function() {
				map.fitBounds(bound);
			}, 750);
		}
	}
} /* Show/hide large Google map */

function toggle_jumbo_map() {
	
	if (jQuery("body").hasClass("mapopen")) {
		jQuery("body").removeClass("mapopen");
		jQuery(".content__mapper").fadeOut(250);
		jQuery(".content__wrapper").css({
			'height': 'auto'
		});
		jQuery("body,html").animate({
			'scrollTop': 0
		}, 500);
	} else {
		
		jQuery("body").addClass("mapopen");
		jQuery(".content__mapper,.content__wrapper").css({
			'height': jQuery(window).height()
		});
		jQuery(".content__mapper").fadeIn(250);
		jQuery("body,html").animate({
			'scrollTop': jQuery(".content__mapper").offset().top
		}, 500);
		
		init_jumbo_map();
	}
} /* Load presentation templates */
var presentationLoaded = false;

function init_presentation() {
	// Show presentation placeholder
	jQuery(".header__top,.header__nav,.nieuwsbrief_bar,.header__visual__interaction a,.content,.footer,.content__wrapper__back").fadeOut(500, function() {
		jQuery(".header,.header__visual--small").animate({
			'height': '100vh'
		}, 250);
		jQuery(".header__visual--small").addClass("presentation-mode");
		jQuery(".site__presentation").fadeIn(300);
		jQuery(".start_logo, .start_nav").fadeIn(300);
	});
	if (presentationLoaded == false) {
		// Load presentation content
		jQuery.post("/pres/", {}, function(results) {
			jQuery(".site__presentation .inner").html(results);
			jQuery(".start_nav").on("click", function() {
				jQuery(".start_nav").addClass("hidenow");
				jQuery(".site__presentation .loader").fadeOut(250, function() {
					jQuery(".site__presentation .inner").fadeIn(250);
				});
				run_presentation();
			});
			presentationLoaded = true;
		});
	} else {
		jQuery(".start_nav").on("click", function() {
			jQuery(".start_nav").addClass("hidenow");
			jQuery(".site__presentation .loader").fadeOut(250, function() {
				jQuery(".site__presentation .inner").fadeIn(250);
			});
		});
	}
}

function close_presentation() {
	jQuery(".site__presentation").fadeOut(300, function() {
		jQuery(".site__presentation .inner").hide();
		jQuery(".site__presentation .loader").show();
		jQuery(".start_logo, .start_nav").hide();
		jQuery(".header__top,.header__nav,.nieuwsbrief_bar,.header__visual__interaction,.content,.footer,.content__wrapper__back").fadeIn(300);
		jQuery(".header,.header__visual--small").animate({
			'height': '670px'
		}, 250);
		jQuery(".header__visual--small").removeClass("presentation-mode");
	});
}

function load_presentation() {
	jQuery("a.header__visual__interaction__button").on("click", function() {
		init_presentation();
		return false;
	});
}

/* Share on social media hooks */
function social_media_popup(width,height,type,title,url) {
    'use strict';
    setTimeout(function() {
	    var leftPosition, topPosition,windowFeatures,s,u,t;
	    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
	    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
	    windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + 
	    				 ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + 
	    				 ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
	    s = location.href;
		if (!title || !url) { u=location.href; t=document.title; } 
		else { u=url; t=title; }
	    if (type=="linkedin") {
			window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(u)+'&title='+encodeURIComponent(t)+'&summary=&source='+encodeURIComponent(s),'sharer', windowFeatures);
		}
		if (type=="twitter") {
			window.open('https://twitter.com/intent/tweet?url='+encodeURIComponent(u)+'&amp;text='+encodeURIComponent(t),'sharer', windowFeatures);
		}
		if (type=="googleplus") {
			window.open('https://plus.google.com/share?url='+encodeURIComponent(u),'sharer',windowFeatures);
		}
	    if (type=="facebook") {
	        window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(u),'sharer',windowFeatures);
	    }
	    if (type=="pinterest") {
	        window.open('https://pinterest.com/pin/create/button/?url='+encodeURIComponent(u),'sharer',windowFeatures);
	    }
	    return false;
	}, 500);
}

function init_breadcrumb_text() {
	'use strict';
	
	if ( jQuery('.content__wrapper__top__text:visible').length ) {
		var textWidth = jQuery('.content__wrapper__top__text:visible').width()+20;
		jQuery('.content__wrapper__top__text:visible').css({'margin-top':textWidth});
	}
	
}

function scroll_content_to_top() {
	'use strict';
	
	jQuery('body,html').animate({'scrollTop':0},500);
	
	return false;
	void(0);
}

var subtitle_original;

function init_menu_subtitle_effect() {
	'use strict';
	
	subtitle_original = jQuery('.header__visual__interaction h2').text();
	
	jQuery('.header__visual__navitems').on('mouseover', 
		function() { 
			jQuery('.header__visual__interaction__button, .header__visual__interaction h3').stop().animate({'opacity':0},250);
		}
	);
	
	jQuery('.header__visual__navitems').on('mouseout', 
		function() { 
			jQuery('.header__visual__interaction__button, .header__visual__interaction h3').stop().animate({'opacity':1},250);
		}
	);
	
	jQuery('.header__visual__navitem a').on('mouseover', 
		function() {
			var subtitle_replace = jQuery(this).attr('data-subtitle');
			jQuery('.header__visual__interaction h2').stop().animate({'opacity':0,'margin-left':'50px'},250, function() { 
				jQuery('.header__visual__interaction h2').text(subtitle_replace);
				jQuery('.header__visual__interaction h2').stop().animate({'opacity':1,'margin-left':'0px'},250);
			});

		}
	);

	jQuery('.header__visual__navitem a').on('mouseout', 
		function() {
			jQuery('.header__visual__interaction h2').stop().animate({'opacity':0,'margin-left':'50px'},250, function() { 
				jQuery('.header__visual__interaction h2').text(subtitle_original);
				jQuery('.header__visual__interaction h2').stop().animate({'opacity':1,'margin-left':'0px'},250);
			});
		}
	);


}

function bwh_disable_button_after_submit() {
	'use strict';
	
	jQuery('.gform_footer button').click(function(){
		jQuery(this).attr("disabled", true).css({'opacity':.5});
		jQuery(this).parent().parent().submit();
	});
	
}


/*
FUNCTION TRIGGERS (RUN ON RESIZE, LOAD OR READY)
*/
jQuery(window).on('scroll', function(e) {});
jQuery(window).resize(function() {
	'use strict';
	init_header_height();
});
jQuery(window).load(function() {
	'use strict';
	init_google_mini_map();
	load_presentation();
});
jQuery(document).ready(function() {
	'use strict';
	init_menu_subtitle_effect()
	init_switch_menu();
	init_search_module();
	init_header_height();
	set_ie_version();
	start_slick_sliders();
	init_fullclick_enabler();
	init_waypoints_header();
	init_breadcrumb_text();
	bwh_disable_button_after_submit();
	
	/* TWITTER */
	
	var twitter = jQuery.ajax({ 
    url: '/wp-admin/admin-ajax.php',
    type: 'GET',
    data: 'action=twitter_feed',
    success: function(results) {
      jQuery("#twitter").removeClass("loading");
      jQuery("#twitter").html(results);
	  jQuery(".tweetslider").slick({
		"autoplay": true,
		"slidesToShow": 1,
		"slidesToScroll": 1,
		"dots": true,
		"fade": false,
		"infinite": true,
		"autoplaySpeed": 5000,
		"adaptiveHeight": true,
		"arrows": false,
		"swipeToSlide": true,
		"customPaging": function(slider, i) {
			return '<button type="button" style="float:left; display: inline-block; background: none; border: none; appearance:none; -moz-appearance:none; /* Firefox */ -webkit-appearance:none; /* Safari and Chrome */" data-role="none">&bull;</button>';
		},
	});
    }
    
    
    
  });
	
	
});