/**
 * Author: Jelle Wielsma (studiodub.nl)
 * jQuery Simple Parallax Plugin
 *
 */

(function($) {
	
    $.fn.parallax = function(options) {
	    
        var windowHeight = $(window).height();
        var settings = $.extend({
            speed        : 0.15,
            modify: 'position',
        }, options);
        
        return this.each( function() {
	        var $this = $(this);

			$(document).scroll(function(){
				var scrollTop = $(window).scrollTop();
	            var offset = $this.offset().top-110;
	            var height = $this.outerHeight();
				
				// Check if above or below viewport
				if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) { return; }
				
				var yBgPosition = Math.round((offset + scrollTop) * settings.speed);
	
				if (settings.modify == 'position') {
		            $this.css({'top':yBgPosition+'px'});
		        } else if (settings.modify == 'background') {
			        $this.css({'background-position':'center '+yBgPosition+'px'});
		        }
				//$this.css('background-position', 'center ' + yBgPosition + 'px');
                
        	});

    	});

    }
    
}(jQuery));