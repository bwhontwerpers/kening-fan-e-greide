<?php
/* Custom post types for this project */

/* 
Custom taxonomies for this project
*/
function bwh_register_taxonomies() {

	$labels = array(
		'name'                       => _x( 'Subjects', 'Taxonomy General Name', 'kfeg' ),
		'singular_name'              => _x( 'Subject', 'Taxonomy Singular Name', 'kfeg' ),
		'menu_name'                  => __( 'Subjects', 'kfeg' ),
		'all_items'                  => __( 'All subjects', 'kfeg' ),
		'parent_item'                => __( 'Parent subject', 'kfeg' ),
		'parent_item_colon'          => __( 'Parent subject:', 'kfeg' ),
		'new_item_name'              => __( 'New subject name', 'kfeg' ),
		'add_new_item'               => __( 'Add new subject', 'kfeg' ),
		'edit_item'                  => __( 'Edit subject', 'kfeg' ),
		'update_item'                => __( 'Update subject', 'kfeg' ),
		'view_item'                  => __( 'View subject', 'kfeg' ),
		'separate_items_with_commas' => __( 'Subjects seperated by commas', 'kfeg' ),
		'add_or_remove_items'        => __( 'Add or remove subjects', 'kfeg' ),
		'choose_from_most_used'      => __( 'Choose from most used subjects', 'kfeg' ),
		'popular_items'              => __( 'Popular subjects', 'kfeg' ),
		'search_items'               => __( 'Search subjects', 'kfeg' ),
		'not_found'                  => __( 'No subjects found', 'kfeg' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);

	register_taxonomy( 'subject', array( 'post','page' ), $args );
	
	$labels = array(
		'name'                       => _x( 'Regio\'s', 'Taxonomy General Name', 'kfeg' ),
		'singular_name'              => _x( 'Regio', 'Taxonomy Singular Name', 'kfeg' ),
		'menu_name'                  => __( 'Regio\'s', 'kfeg' ),
		'all_items'                  => __( 'Alle regio\'s', 'kfeg' ),
		'parent_item'                => __( 'Bovenliggend Regio', 'kfeg' ),
		'parent_item_colon'          => __( 'Bovenliggend Regio:', 'kfeg' ),
		'new_item_name'              => __( 'Naam nieuw Regio', 'kfeg' ),
		'add_new_item'               => __( 'Nieuw Regio toevoegen', 'kfeg' ),
		'edit_item'                  => __( 'Bewerk Regio', 'kfeg' ),
		'update_item'                => __( 'Regio bijwerken', 'kfeg' ),
		'view_item'                  => __( 'Bekijk regio', 'kfeg' ),
		'separate_items_with_commas' => __( 'Regio\'s, gescheiden door komma\'s', 'kfeg' ),
		'add_or_remove_items'        => __( 'Regio\'s toevoegen of verwijderen', 'kfeg' ),
		'choose_from_most_used'      => __( 'Kies uit de meest gebruikte Regio\'s', 'kfeg' ),
		'popular_items'              => __( 'Populaire Regio\'s', 'kfeg' ),
		'search_items'               => __( 'Zoek regio\'s', 'kfeg' ),
		'not_found'                  => __( 'Geen regio\'s gevonden', 'kfeg' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);

	register_taxonomy( 'regio', array( 'post','page' ), $args );

}
add_action( 'init', 'bwh_register_taxonomies', 0 );


/* Add new rewrite rules to enable 'filter' query_var */
function article_filter_rules() {
	add_rewrite_rule('filter/?([^/]*)', 'index.php?filtervalue=test', 'top');
//	add_rewrite_rule('filter/?([^/]*)', 'index.php?post_type=page&filter=$matches[1]', 'top');
//	add_rewrite_rule('filter/?([^/]*)', 'index.php?post_type=page&weergave='.$matches[1], 'top');
//	add_rewrite_rule('filter/?([^/]*)/list', 'index.php?post_type=post&weergave=lijst&filter=$matches[1]', 'top');
//	add_rewrite_rule('?([^/]*)/map', 'index.php?post_type=project&weergave=grid&filter=$matches[1]', 'top');
} //add_action('init', 'article_filter_rules');
function article_filter_query_vars($vars) {
//	$vars[] = 'weergave';
	$vars[] = 'filtervalue';
	return $vars;
} //add_filter('query_vars', 'article_filter_query_vars');
?>