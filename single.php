<?php /* Single post */ 

while (have_posts()) { the_post(); 
	$categories = get_the_category();
	foreach ($categories as $category) {
		if ($category->term_id != 2) {
			get_template_part("content",$category->slug);
			break;
		}
	}
}

?>