<?php 
/* The template for displaying the footer */ 
?>
	<div class="row nieuwsbrief_bar dynamic_width">
		<div class="section">
			<!--<div class="col col_1 nomobile">&nbsp;</div>-->
			<div class="col col_14 col_first nieuwsbrief_bar_wrapper">
				<?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
			</div>
			<div class="col col_2 col_first">
				<div class="social__share social__share--footer">
					<a class="social__share__icon" target="_blank" href="https://www.facebook.com/KeningfaneGreide/" title="Facebook">
						<?php bstcm_load_image('kfeg_picto-facebook.svg',false,false,true); ?>
					</a>
					<a class="social__share__icon social__share__icon--twitter" target="_blank" href="https://twitter.com/keninggreide" title="Twitter">
						<?php bstcm_load_image('kfeg_picto-twitter.svg',false,false,true); ?>
					</a>
				</div>
			</div>
			<!--<div class="col col_1 nomobile">&nbsp;</div>-->
		</div>
	</div>
	    	    
		<div class="row footer">
		</div>
	    	
	</div>
	
	<div class="desktop"></div>
	<div class="tablet"></div>
	<div class="tabletlandscape"></div>
	<div class="tabletportrait"></div>
	<div class="smartphone"></div>

	<?php wp_footer(); ?>

</body>
</html>