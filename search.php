<?php
/* Search results */
get_header();
global $query_string;
query_posts($query_string . '&showposts=20');
?>

	<div class="row content">
		
		<div class="section">
			<div class="col col_16 col_first dynamic_width">
				<div class="content__wrapper">
					<div class="site_overlay"></div>
					<h1 class="h1--archive">Zoekresultaten</h1>
					<h3 class="h3--subtitle"><?php the_search_query(); ?></h3>
										
					<div class="content__filter__items" data-columns data-custom-filter="<?php echo $customfilter; ?>">
						<?php get_template_part("modules/get","search-items"); ?>
					</div>
										
				</div>
				
			</div>
		</div>
		
	</div>
	
<?php get_footer(); ?>